import { ValidationErrors, ValidatorFn, AbstractControl } from '@angular/forms';

export class CustomValidators {
    /**
     * Static method for validating fields of the forms
     * using reg exp
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @param regex RegExp
     * @param error ValidationErrors
     * @platform contractorsbids
     */
    static patternValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
        return (control: AbstractControl): {[key: string]: any} => {
            if (!control.value) {
                // if control is empty return no error
                return null;
            }

            // test the value of the control against the regexp supplied
            const valid = regex.test(control.value);

            // if true, return no error (no error), else return error passed in the second parameter
            return valid ? null : error;
        };
    }

    /**
     * Static Method to validate that the password match in
     * both field at the moment to register the user
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @param control AbstractControl
     * @platform contractorsbids
     */
    static passwordMatchValidator(control: AbstractControl) {
        // get password from our password form control
        const password: string  = control.get('password').value;
        // get password from our confirmPassword form control
        const cpassword: string = control.get('cpassword').value;

        // compare is the password math
        if (password !== cpassword) {
            // if they don't match, set an error in our confirmPassword form control
            control.get('cpassword').setErrors({ NoPasswordMatch: true });
        }
    }

    /**
     * Static Method for check if the emails are the
     * same, if there aren't same will display the errors
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @param control AbstractControl
     * @platform contractorsbids
     */
    static emailMatchValidator(control: AbstractControl) {
        // get email from our email control
        const email: string = control.get('email').value;
        //get email from our confirm_email form control
        const confirm_email: string = control.get('confirm_email').value;

        // compare if the emails are equals
        if (email !== confirm_email) 
        {
            // if they don't match, set an error in our confirm email form control
            control.get('confirm_email').setErrors({ NoEmailMatch: true });
        }
    }
}