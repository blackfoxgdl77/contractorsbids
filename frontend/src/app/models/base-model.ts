export class BaseModel {
  id            : number;
  date_created  : string;
  date_updated  : string;

  constructor() {
    this.date_created = new Date().toLocaleString();
    this.date_updated = new Date().toLocaleString();
  }

}
