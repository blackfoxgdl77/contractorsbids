export class Users {
    id               : number;
    name             : string;
    email            : string;
    username         : string;
    password         : string;
    is_company       : number;
    type_user        : number;
    is_active        : number;
    is_admin         : boolean;
    activation_token : string;
    is_confirm_token : boolean;
    date_created     : string;
    date_updated     : string;
    email_base       : string;
}
