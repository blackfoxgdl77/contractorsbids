import { BaseModel } from './base-model';

export class City extends BaseModel {
  name          : string;
  capital       : string;

}
