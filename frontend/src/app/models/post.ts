import { BaseModel } from './base-model';

export class Post extends BaseModel {
  post_name         : string;
  description       : string;
  post_payment      : number;
  category          : number;
  city              : number;
  user              : number;
  post_status       : number;
  payment_history   : number;
  attachments       : Array<number>;
  offers            : Array<number>;

}
