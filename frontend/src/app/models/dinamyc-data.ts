import { BaseModel } from './base-model';

export class DinamycData extends BaseModel {
    section       : number;
    description   : string;
    file          : string;
    file_size     : string;
    original_name : string;
    url           : string;
    type_file     : number;
    file_extension: string;
    status        : number;
}
