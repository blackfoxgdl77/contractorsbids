import { BaseModel } from './base-model';

export class Publish extends BaseModel {
  user      : number;
  post      : number;
  owner     : number;
  comments  : string;
  title     : string;

}
