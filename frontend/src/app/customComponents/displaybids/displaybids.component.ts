import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { AuthService } from 'src/app/services/auth.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-displaybids',
    templateUrl: './displaybids.component.html',
    styleUrls: ['./displaybids.component.scss']
})
export class DisplaybidsComponent implements OnInit {
    @Input() post       : number;
    @Input() postStatus : number;

    @Output() backObj   : EventEmitter<any>    = new EventEmitter<any>();

    codeResponse    : string;
    statusResponse  : string;
    messageResponse : string;
    bidsData        : any[];
    totalRecords    : number;
    headerResponse  : string;
    flagSuccess     : boolean;
    flagError       : boolean;
    username        : string;

    constructor(private api: ApiService,
                private auth: AuthService) {
                 }

    ngOnInit() {
        // get bids per project
        return this.api.getRequest(`/api/get/${this.post}/${this.auth.number}/projects/bids`)
                .subscribe(res => {
                    if (res.body['status'] == 'OK' && res.body['code'] == 200) {
                        let returnedData = res.body['data'];

                        this.username        = (returnedData[0].status == 2) ? returnedData[0].username : '';
                        this.codeResponse    = res.body['code'];
                        this.statusResponse  = res.body['status'];
                        this.messageResponse = res.body['message'];
                        this.bidsData        = returnedData;
                        this.totalRecords    = (this.bidsData !== undefined) ? this.bidsData.length : 0;
                    }

                    if (res.body['status'] == 'ERROR' && res.body['code'] == 400) {
                        this.statusResponse  = res.body['status'];
                        this.codeResponse    = res.body['code'];
                        this.messageResponse = res.body['message'];
                    }
                });
    }

    /**
     * Add the informationfor accept the bid or simply
     * decline the bid
     * 
     * @param status integer
     * @param bid integer
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    aceptDeclineBid(status: number, bid: number) : void {
        let obj = { 'status' : status,
                    'bid'    : bid };
                
        this.api.postRequest(`/api/bid/accept`, JSON.stringify(obj))
                .subscribe(res => {
                    if (res.body['code'] == 200 && res.body['status'] == 'OK') {
                        let returnedData = res.body['data'];

                        localStorage.setItem('userT1', res.headers.get('_token_sec'));
                        localStorage.setItem('userT2', res.headers.get('_token_log'));
                        localStorage.setItem('number', res.headers.get('_token_user'));

                        this.messageResponse = res.body['message'];
                        this.statusResponse  = res.body['status'];
                        this.codeResponse    = res.body['code'];
                        this.headerResponse  = res.body['header'];
                        this.flagSuccess     = true;
                        this.username        = returnedData['username'];

                        this.emitResults(returnedData);
                    }

                    if (res.body['code'] == 400 && res.body['status'] == 'ERROR') {
                        this.messageResponse = res.body['message'];
                        this.statusResponse  = res.body['status'];
                        this.codeResponse    = res.body['code'];
                        this.headerResponse  = '';
                        this.flagError       = true;

                        this.emitResults(null)
                    }
                });
    }

    /**
     * Emit the action once the response has been received by the back
     * 
     * @params data obj
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    private emitResults(data: any): void {
        this.backObj.emit({ 
            'messageResponse' : this.messageResponse,
            'headerResponse'  : this.headerResponse,
            'codeResponse'    : this.codeResponse,
            'statusResponse'  : this.statusResponse,
            'flagSuccess'     : this.flagSuccess,
            'flagError'       : this.flagError,
            'data'            : data
        });
    }
}
