import { Component } from '@angular/core';
import { SuiModal, ComponentModalConfig, ModalSize } from 'ng2-semantic-ui';

@Component({
    selector: 'modal-accept-reject',
    template: `<div class="header">{{ modal.context.title }}</div>
    <div class="content">
        <p>{{ modal.context.body }}</p>
    </div>
    <div class="actions">
        <button class="ui red button" (click)="modal.deny()">{{ modal.context.deny }}</button>
        <button class="ui green button" (click)="modal.approve()">{{ modal.context.accept }}</button>
    </div>`
})
export class AcceptRejectModalComponent {
    constructor(public modal: SuiModal<IAcceptRejectModalContext, void, void>) {}
}