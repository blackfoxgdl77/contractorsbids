import { Component, OnInit } from '@angular/core';
import { Route, Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { IMessage } from 'ng2-semantic-ui';

@Component({
    selector: 'app-confirm-message',
    templateUrl: './confirm-message.component.html',
    styleUrls: ['./confirm-message.component.scss']
})
export class ConfirmMessageComponent implements OnInit {
    paramString : any;
    username    : string;
    status      : string;
    code        : number;
    message     : string;
    isActive    : number;
    userId      : number;

    title  : string = "Activate Account";
    breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'Activate Account', link: '', isLink: false }
    ];

    steps : Object[] = [
        { title: 'Create Account', icons: 'address card', description: 'Create New Account', completed: 'completed' },
        { title: 'Email Confirmation', icons: 'check circle', description: 'Receive Email Confirmation', completed: 'completed' },
        { title: 'Confirm Account', icons: 'check circle outline', description: 'Confirm and Active your Account', completed: '' }
    ];

    constructor(private api: ApiService,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.paramString = this.activatedRoute.snapshot.paramMap.get('token_confirm');

        this.api.getRequest(`/api/users/activate/${this.paramString}/account`)
                .subscribe((res: any) => {
                    // TODO: Implementing observables
                    if (res['body'].code == 200 && res['body'].status == 'OK')
                    {
                        let returnedData = res['body'].data;

                        this.status   = res['body'].status;
                        this.code     = res['body'].code;
                        this.message  = returnedData.message;
                        this.username = returnedData.username;
                        this.userId   = returnedData.user_id;
                        this.isActive = returnedData.active;
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR')
                    {
                        this.status  = res['body'].status;
                        this.code    = res['body'].code;
                        this.message = res['body'].message;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

}
