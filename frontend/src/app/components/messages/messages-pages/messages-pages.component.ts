import { Component, OnInit } from '@angular/core';
import { BreadcrumbsComponent } from 'src/app/components/breadcrumbs/breadcrumbs.component';
import { IMessage } from 'ng2-semantic-ui';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-messages-pages',
  templateUrl: './messages-pages.component.html',
  styleUrls: ['./messages-pages.component.scss']
})
export class MessagesPagesComponent implements OnInit {
    paramId   : any;
    paramName : any;

    title  : string = "Account Created!";
    breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'Create Account', link: '/auth/register', isLink: true },
        { text: 'Account Created!', link: '', isLink: false }
    ];
    steps : Object[] = [
        { title: 'Email', icons: 'envelope', description: 'Receive an Email' },
        { title: 'Confirmation', icons: 'check circle', description: 'Confirm Your Account' },
        { title: 'Start to Publish', icons: 'pencil alternate', description: 'Start to Publish Your Projects' }
    ];

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.paramId   = this.activatedRoute.snapshot.queryParams.id;
        this.paramName = this.activatedRoute.snapshot.queryParams.user;
    }

}
