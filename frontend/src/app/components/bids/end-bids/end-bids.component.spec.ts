import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndBidsComponent } from './end-bids.component';

describe('EndBidsComponent', () => {
  let component: EndBidsComponent;
  let fixture: ComponentFixture<EndBidsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndBidsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndBidsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
