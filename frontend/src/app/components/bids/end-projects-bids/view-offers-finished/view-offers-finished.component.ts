import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ActivatedRoute } from '@angular/router';
import { IMessage } from 'ng2-semantic-ui';
import { AuthService } from 'src/app/services/auth.service';

@Component({
    selector: 'app-view-offers-finished',
    templateUrl: './view-offers-finished.component.html',
    styleUrls: ['./view-offers-finished.component.scss']
})
export class ViewOffersFinishedComponent implements OnInit {
    paramId         : any;
    codeResponse    : number  = 0;
    statusResponse  : string  = '';
    messageResponse : string  = '';
    myBidsFinished  : [];
    totalRecords    : number;
    flagError       : boolean = false;
    flagSuccess     : boolean = false;

    title  : string = "List of Bids - Finished";
    breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'My End Bids Projects', link: '/bids/end-projects-bids/view-all-end-projects', isLink: true },
        { text: 'List of Bids', link: '', isLink: false }
    ];
    steps : Object[] = [
        { title: 'Review the Bids', icons: 'eye', description: 'Review the bids published', completed: 'completed' },
        { title: 'Choose a Bid', icons: 'money', description: 'Choose the best bid', completed: 'completed' },
        { title: 'Get it done!', icons: 'check circle outline', description: 'Work the project once you selected the bid!', completed: 'completed' }
    ];

    constructor(private api: ApiService,
                private auth: AuthService,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.paramId = this.activatedRoute.snapshot.paramMap.get('id');

        this.api.getRequest(`/api/get/${this.paramId}/${this.auth.number}/projects/bids`)
                .subscribe((res: any) => {
                    // TODO: Implementing observables
                    if (res['body'].code == 200 && res['body'].status == 'OK')
                    {
                        let responseData = res['body'].data;

                        this.myBidsFinished  = responseData;
                        this.totalRecords    = this.myBidsFinished.length;
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.flagSuccess     = true;
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR')
                    {
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.flagError       = true;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Method will be used to close the baners are displayed on the
     * list
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public dismiss(message:IMessage)
    {
        this.messageResponse = '';
        this.statusResponse  = '';
        this.codeResponse    = 0;
    }
}
