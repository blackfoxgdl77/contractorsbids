import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewOffersFinishedComponent } from './view-offers-finished.component';

describe('ViewOffersFinishedComponent', () => {
  let component: ViewOffersFinishedComponent;
  let fixture: ComponentFixture<ViewOffersFinishedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewOffersFinishedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewOffersFinishedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
