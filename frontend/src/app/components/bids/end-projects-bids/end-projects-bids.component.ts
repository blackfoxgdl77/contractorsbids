import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-end-projects-bids',
    templateUrl: './end-projects-bids.component.html',
    styleUrls: ['./end-projects-bids.component.scss']
})
export class EndProjectsBidsComponent implements OnInit {

    constructor(private api: ApiService,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
    }
}
