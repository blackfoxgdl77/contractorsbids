import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { AuthService } from 'src/app/services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { IMessage } from 'ng2-semantic-ui';

@Component({
    selector: 'app-view-all-my-bids-projects',
    templateUrl: './view-all-my-bids-projects.component.html',
    styleUrls: ['./view-all-my-bids-projects.component.scss']
})
export class ViewAllMyBidsProjectsComponent implements OnInit {
    myBidsActive    : [];
    paramId         : any;
    codeResponse    : number;
    messageResponse : string;
    headerResponse  : string;
    statusResponse  : string;
    flagError       : boolean = false;
    flagSuccess     : boolean = false;

    title  : string = "My Project Bids - List";
    breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'My Project Bids', link: '/bids/projects-bids/view-all-bids-project', isLink: true },
        { text: 'List all my Project Bids', link: '', isLink: false }
    ];
    steps : Object[] = [
        { title: 'Review the Bids', icons: 'eye', description: 'Review the bids published', completed: 'completed' },
        { title: 'Choose a Bid', icons: 'money', description: 'Choose the best Bid' },
        { title: 'Get it done!', icons: 'check circle outline', description: 'Work the project once you selected the bid!' }
    ];

    constructor(private api: ApiService,
                private auth: AuthService,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.paramId = this.activatedRoute.snapshot.paramMap.get('id');
        this.api.getRequest(`/api/get/${this.paramId}/${this.auth.number}/projects/bids`)
                .subscribe(res => {
                    if (res.body['code'] == 200 && res.body['status'] == 'OK') {
                        let returnedData = res.body['data'];

                        this.myBidsActive    = returnedData;
                        this.messageResponse = res.body['message'];
                        this.statusResponse  = res.body['status'];
                        this.codeResponse    = res.body['code'];
                        this.headerResponse  = res.body['header'];
                    }

                    if (res.body['code'] == 400 && res.body['status'] == 'ERROR') {
                        this.messageResponse = res.body['message'];
                        this.statusResponse  = res.body['status'];
                        this.codeResponse    = res.body['code'];
                        this.flagError       = true;
                    }
                });
    }

    /**
     * Method will be used to accept or decline a bid
     * done by the user to do the project
     * 
     * @param status integer
     * @param bids object
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    actionBidOwner(status: number, bids: any): void {
        let actions = { 'status' : status,
                        'bid'    : bids.bid };

        this.api.postRequest(`/api/bid/accept`, JSON.stringify(actions))
                .subscribe((res: any) => {
                    // TODO: Implementing observables
                    if (res['body'].code == 200 && res['body'].status == 'OK')
                    {
                        let returnedData = res['body'].data;

                        localStorage.setItem('userT1', res.headers.get('_token_sec'));
                        localStorage.setItem('userT2', res.headers.get('_token_log'));
                        localStorage.setItem('number', res.headers.get('_token_user'));

                        this.messageResponse = res['body'].message;
                        this.statusResponse  = res['body'].status;
                        this.codeResponse    = res['body'].code;
                        this.headerResponse  = res['body'].header;
                        this.flagSuccess     = true;
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR')
                    {
                        this.messageResponse = res['body'].message;
                        this.statusResponse  = res['body'].status;
                        this.codeResponse    = res['body'].code;
                        this.flagError       = true;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Method will be used to close the banners are displayed on the
     * list
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public dismiss(message:IMessage)
    {
        this.flagError       = false;
        this.flagSuccess     = false;
        this.codeResponse    = 0;
        this.statusResponse  = '';
        this.messageResponse = '';
        this.headerResponse  = '';
    }
}
