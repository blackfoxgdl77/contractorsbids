import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAllMyBidsProjectsComponent } from './view-all-my-bids-projects.component';

describe('ViewAllMyBidsProjectsComponent', () => {
  let component: ViewAllMyBidsProjectsComponent;
  let fixture: ComponentFixture<ViewAllMyBidsProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAllMyBidsProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAllMyBidsProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
