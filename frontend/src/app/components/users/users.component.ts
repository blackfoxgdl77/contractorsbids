import { Component, OnInit } from '@angular/core';
import { BreadcrumbsComponent } from 'src/app/components/breadcrumbs/breadcrumbs.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  title  : string = 'Personal Information';
  breads : Object[] = [
      { text: 'Home', link: '/', isLink: true },
      { text: 'Personal Information', link: '', isLink: false }
  ];

  constructor() { }

  ngOnInit() {
  }

}
