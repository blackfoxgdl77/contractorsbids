import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { AcceptRejectModal } from 'src/app/helpers/AcceptRejectModal';
import { SuiModalService, IMessage } from 'ng2-semantic-ui';
import { AuthService } from 'src/app/services/auth.service';

@Component({
    selector: 'app-list-all-jobs-reported',
    templateUrl: './list-all-jobs-reported.component.html',
    styleUrls: ['./list-all-jobs-reported.component.scss']
})
export class ListAllJobsReportedComponent implements OnInit {
    title  : string = "Jobs Reported Fake";
    breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'Jobs Reported', link: '/jobs-reported/list-all-fake-jobs', isLink: true },
        { text: 'Jobs Reported Fake', link: '', isLink: false }
    ];

    flagSuccessRecord : boolean = false;
    flagDeleteRecord  : boolean = false;
    fakeJobs          : [];
    totalRecords      : number;
    codeResponse      : number;
    messageResponse   : string = '';
    messageStatus     : string = '';
    headerMessage     : string = '';

    constructor(private api: ApiService,
                private auth: AuthService,
                private modalService: SuiModalService) { }

    ngOnInit() {
        this.api.getRequest(`/api/fake/get/all`)
                .subscribe((res: any) => {
                    // TODO: Implementing observables
                    if (res['body'].code == 200 && res['body'].status == 'OK')
                    {
                        let dataRecovery = res['body'].data;

                        this.totalRecords    = dataRecovery.length;
                        this.fakeJobs        = dataRecovery;
                        this.codeResponse    = res['body'].code;
                        this.messageStatus   = res['body'].status;
                        this.messageResponse = res['body'].message;

                    }

                    if(res['body'].code == 400 && res['body'].status == 'ERROR')
                    {
                        this.messageStatus   = res['body'].status;
                        this.codeResponse    = res['body'].code;
                        this.messageResponse = res['body'].message;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Soft-delete the information selected by the user once clicks
     * the delete button
     * 
     * @param title string
     * @param message string
     * @param id number
     * @param id_project number
     * @param button1 string
     * @param button2 string
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    deleteRecord(title: string, message: string, id: number, button1: string, button2: string) : void {
        let modal = new AcceptRejectModal(title, message, button1, button2);

        this.modalService
            .open(modal)
            .onApprove(() => {
                let dataPost  = { 'userS' : this.auth.userT1,
                                  'userL' : this.auth.userT2,
                                  'userN' : this.auth.number,
                                  'log'   : this.auth.historyLogin }; 

                this.api.postRequest(`/api/fake/${id}/delete`, JSON.stringify(dataPost))
                    .subscribe((res: any) => {
                        // TODO: Implementing obsravbles
                        if (res['body'].code == 200 && status == 'OK') {
                            let data = res['body'].data;

                            this.auth.sec_tokens(data.s_token);

                            this.flagSuccessRecord = true;
                            this.messageStatus     = res['body'].status;
                            this.messageResponse   = res['body'].message;
                            this.codeResponse      = res['body'].code;
                            this.headerMessage     = res['body'].header;
                        }

                        if (res['body'].code == 400 && res['body'].status == 'ERROR') {
                            this.flagDeleteRecord = true;
                            this.codeResponse     = res['body'].code;
                            this.messageStatus    = res['body'].status;
                            this.messageResponse  = res['body'].message;
                        }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
            })
            .onDeny(() => {});
    }

    /**
     * Method for active or inactive records on the admin section
     * that will be displayed on the page for all the users
     * 
     * @param title string
     * @param message string
     * @param id number
     * @param id_project number
     * @param value number
     * @param button1 string
     * @param button2 string
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public open(title: string, message: string, id: number, status: number, value: number, button1: string, button2: string)
    {
        const modal = new AcceptRejectModal(title, message, button1, button2);

        this.modalService
            .open(modal)
            .onApprove(() => this.modalOptions(id, status, value))
            .onDeny(() => {});
    }

    /**
     * Method for make a request once we get the url
     * used for make the call to the api created
     * 
     * @param id number
     * @param status number
     * @param value number
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    private modalOptions(id: number, status: number, value: number): void
    {
        let urlString = this.stringRequest(id, status, value);

        let dataPost  = { 'userS' : this.auth.userT1,
                          'userL' : this.auth.userT2,
                          'userN' : this.auth.number,
                          'log'   : this.auth.historyLogin }; 

        this.api.postRequest(urlString, JSON.stringify(dataPost))
                .subscribe((res: any) => {
                    // TODO: Implementing observables
                    if (res['body'].code == 200 && res['body'].status == 'OK')
                    {
                        let responseData = res['body'].data;

                        this.auth.sec_tokens(responseData.s_token);

                        this.flagSuccessRecord = true;
                        this.codeResponse      = res['body'].code;
                        this.messageStatus     = res['body'].status;
                        this.messageResponse   = res['body'].message;
                        this.headerMessage     = res['body'].header;
                    }

                    if (res['body'].status == 'ERROR' && res['body'].code == 400)
                    {
                        this.flagDeleteRecord = true;
                        this.messageStatus    = res['body'].status;
                        this.codeResponse     = res['body'].code;
                        this.messageResponse  = res['body'].message;
                    }
                },
                (error: any) => {
                    // TODO: What code should be implemented?
                },
                () => {
                    // TODO: What code should be implemented?
                }); 
    }

    /**
     * Private method will be used for get the url will be
     * called the function once the modal has been approved
     * 
     * @param id number
     * @param status number
     * @param value number
     * @return string
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    private stringRequest(id, status, value): string
    {
        let stringRequest;
        switch(value) {
            case 1: // Active
                stringRequest = `/api/fake/${id}/1/status`;
                break;
            case 2: // Inactive
                stringRequest = `/api/fake/${id}/2/status`;
                break;
        }

        return stringRequest;
    }

    /**
     * Method will be used to dismiss the message displayed in the
     * banner once the user executes an actions such as delete records
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public dismiss(message: IMessage) {
        this.flagDeleteRecord = this.flagSuccessRecord = false;
        
        this.codeResponse    = 0;
        this.messageStatus   = '';
        this.messageResponse = '';
        this.headerMessage   = '';
    }

}
