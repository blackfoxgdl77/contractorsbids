import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { IMessage } from 'ng2-semantic-ui';
import { DinamycData } from 'src/app/models/dinamyc-data';
import { SuiModalService } from 'ng2-semantic-ui';
import { AcceptRejectModal } from 'src/app/helpers/AcceptRejectModal';
import { Observable, Subscription } from 'rxjs';
import { ModalService } from 'src/app/services/modal.service';
import { EmbedVideoService } from 'ngx-embed-video';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {
    public title  : string = "Banners / List Records";
    public breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'Administrator', link: '/admin/administrator', isLink: true },
        { text: 'Banners - List', link: '', isLink: false }
    ];

    public bannersList       : DinamycData[];
    public totalRecords      : number;
    public flagDeleteRecord  : boolean = false;
    public flagSuccessRecord : boolean = false;
    public messageStatus     : string;
    public messageResponse   : string;
    public headerMessage     : string;
    public codeResponse      : number  = 0;
    public type_file         : number;
    public file              : string;

    // Observables and Subscription
    observers    = new Observable();
    subscription : Subscription;

    constructor(private api: ApiService,
                private modal: ModalService,
                private videos: EmbedVideoService,
                private modalService: SuiModalService) { }

    ngOnInit() {
        this.displayAllRecords();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    /**
     * Method used to display all the information stored and created by the
     * user in the admin section
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    displayAllRecords(): void {
        this.observers = this.api.getRequest("/api/admin/1/get/section");
        this.subscription = this.observers.subscribe((res: any) => {
                    this.bannersList  = res['body'].data;
                    this.totalRecords = (this.bannersList !== undefined) ? this.bannersList.length : 0;
                },
                (error: any) => {
                    // TODO: Verify the cde will be put here
                },
                () => { 
                    // TODO: Verify how we can use this function
                });
    }

    /**
     * Soft-delete the information selected by the user once clicks
     * the delete button
     * 
     * @param title string
     * @param message string
     * @param id number
     * @param button1 string
     * @param button2 string
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    deleteRecord(title: string, message: string, id: number, button1: string, button2: string) : void {
        let modal = new AcceptRejectModal(title, message, button1, button2);

        this.modalService
            .open(modal)
            .onApprove(() => {
                this.observers    = this.api.deleteRequest(`/api/admin/${id}/delete`);
                this.subscription = this.observers.subscribe((res: any) => {
                    if (res['body'].code == 200 && res['body'].status == 'OK') {
                        let data = res['body'].data;

                        this.flagSuccessRecord = true;
                        this.messageStatus     = data.status;
                        this.messageResponse   = data.message;
                        this.codeResponse      = data.code;
                        this.headerMessage     = data.header;
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR') {
                        this.flagDeleteRecord = true;
                        this.codeResponse     = res['body'].code;
                        this.messageStatus    = res['body'].status;
                        this.messageResponse  = res['body'].message;
                    }
                },
                (error: any) => {
                    // TODO: What code will be put here?
                },
                () => {
                    // TODO: What should be put here?
                });
            })
            .onDeny(() => {});
    }

    /**
     * Method for active or inactive records on the admin section
     * that will be displayed on the page for all the users
     * 
     * @param title string
     * @param message string
     * @param id number
     * @param value number
     * @param button1 string
     * @param button2 string
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public open(title: string, message: string, id: number, value: number, button1: string, button2: string)
    {
        const modal = new AcceptRejectModal(title, message, button1, button2);

        this.modalService
            .open(modal)
            .onApprove(() => this.modalOptions(id, value))
            .onDeny(() => {});
    }

    /**
     * Method will be used to dismiss the message displayed in the
     * banner once the user executes an actions such as delete records
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public dismiss(message: IMessage) {
        this.flagDeleteRecord  = false;
        this.flagSuccessRecord = false;

        this.codeResponse    = 0;
        this.messageStatus   = '';
        this.messageResponse = '';
        this.headerMessage   = '';
    }

    /**
     * Method for make a request once we get the url
     * used for make the call to the api created
     * 
     * @param id number
     * @param value number
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    private modalOptions(id: number, value: number): void
    {
        let urlString = this.stringRequest(id, value);

        this.observers = this.api.deleteRequest(urlString);
        this.subscription = this.observers.subscribe((res: any) => {
                    if (res['body'].code == 200 && res['body'].status == 'OK')
                    {
                        let responseData = res['body'].data;

                        this.flagSuccessRecord = true;
                        this.codeResponse      = res['body'].code;
                        this.messageStatus     = res['body'].status;
                        this.messageResponse   = responseData.message;
                        this.headerMessage     = responseData.headerMsg;
                    }

                    if (res['body'].status == 'ERROR' && res['body'].code == 400)
                    {
                        this.flagDeleteRecord = true;
                        this.messageStatus    = res['body'].status;
                        this.codeResponse     = res['body'].code;
                        this.messageResponse  = res['body'].message;
                    }
                },
                (error: any) => {
                    // TODO: Verify what could we put of code here
                },
                () => {
                    // TODO: Verify what could we do in this function
                });
    }

    /**
     * Private method will be used for get the url will be
     * called the function once the modal has been approved
     * 
     * @param id number
     * @param value number
     * @return string
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    private stringRequest(id, value): string
    {
        let stringRequest;
        switch(value) {
            case 1: // Active
                stringRequest = `/api/availability/${id}/1/admin`;
                break;
            case 2: // Inactive
                stringRequest = `/api/availability/${id}/2/admin`;
                break;
        }

        return stringRequest;
    }

    /**
     * Method to open the modal once the user clicks the
     * open button to see the file
     * 
     * @param id string
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    openViewerFiles(id: string, type_file: number, file: string): void {
        this.type_file = type_file;
        this.file      = (type_file === 1) ? file : this.videos.embed(file, { attr: { width: 800, height: 400}});
        this.modal.open(id);
    }

    /**
     * Method to close the modal will be used to display
     * the image once the user clicks close button
     * 
     * @param id string
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    closeViewerFiles(id: string): void {
        this.modal.close(id);
    }
}
