import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { ImagesService } from 'src/app/services/images.service';
import { IMessage } from 'ng2-semantic-ui';
import { DinamycData } from 'src/app/models/dinamyc-data';
import { Router, Route } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
    public title  : string = "Banners / Create Record";
    public breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'Administrator', link: '/admin/administrator', isLink: true },
        { text: 'Banners - List', link: '/admin/banners/list', isLink: true },
        { text: 'Banners - Create', link: '', isLink: false }
    ];

    public formGroupAdd : FormGroup;
    public flagSuccessM : boolean = false;
    public flagErrorsM  : boolean = false;
    public submitedForm : boolean = false;
    public filesData    : File    = null;
    public errorStatus  : string  = '';
    public errorMessage : string  = '';
    public errorCode    : number  = 0;
    public flagCreate   : number  = 0;
    public enableFile   : boolean = false;
    public enableUrl    : boolean = false;
    
    public editorConfig = {
        editable: true,
        spellcheck: true,
        height: '15rem',
        minHeight: '5rem',
        placeholder: 'Type a description...',
        translate: 'yes'
    };


    constructor(private api: ApiService, 
                private fb: FormBuilder, 
                private img: ImagesService,
                private _router: Router) { }

    ngOnInit() {
        this.validations();
    }

    /**
     * Method will be used to validate
     * the data of the forms and create a new
     * entrance
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    validations(): void {
        this.formGroupAdd = this.fb.group({
            section     : ['Banners'],
            description : ['', [Validators.required]],
            options     : ['', [Validators.required]],
            status      : ['', [Validators.required]],
            urlVideo    : [''],
            file        : ['']
        });

        this.formGroupAdd.controls['section'].disable();
        this.formGroupAdd.controls['section'].disable();
        this.formGroupAdd.controls['urlVideo'].disable();
        this.formGroupAdd.controls['file'].disable();
    }

    /**
     * Event will be used to get the information of the 
     * image will be uploaded to the server
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    onFilesLoad(event: any) {
        this.filesData = <File>event.target.files;
    }

    /**
     * Method will be used to save the new record related to about us
     * and will be stores on admin table
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    newRecordCreated(): void {
        let values = this.formGroupAdd.value;
        Object.assign(values, { section : 'Banners' });
        this.submitedForm = true;

        if (values['options'] == '1') {
            Object.assign(values, { urlVideo : '' });
        }
        
        if (this.formGroupAdd.invalid) {
            this.flagErrorsM = (this.flagErrorsM) ? this.flagErrorsM : !this.flagErrorsM;

            return ;
        }
        
        this.flagErrorsM = false;
        this.api.postRequest('/api/admin/create', JSON.stringify(values))
                .subscribe(res => {
                    if (res.body['status'] == 'OK' && res.body['code'] == 200)
                    {
                        let returnedData = res.body['data'];

                        localStorage.setItem('userT1', res.headers.get('_token_sec'));
                        localStorage.setItem('userT2', res.headers.get('_token_log'));
                        localStorage.setItem('number', res.headers.get('_token_user'));
                        
                        if (returnedData['options'] == '1')
                        {
                            this.uploadImage(returnedData['id']);
                        } 
                        else 
                        {
                            this.flagSuccessM = true;
                            this.flagCreate   = 1;

                            this.clearFields();
                        }
                    }

                    if (res.body['status'] == 'OK' && res.body['code'] == 400)
                    {
                        this.errorMessage = res.body['message'];
                        this.errorStatus  = res.body['status'];
                        this.errorCode    = res.body['code'];
                        this.flagErrorsM  = true;
                    }
                });
    }

    /**
     * Method will be used for upload image in every
     * section of the admin module
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    uploadImage(id) {
        const formData = new FormData();
        formData.append('file', this.filesData[0]);
        this.img.postImage(`/api/upload/1/${id}/images`, formData)
                .subscribe(res => {
                    if (res['status'] == 'OK' && res['code'] == 200) 
                    {
                        this.flagSuccessM = true;
                        this.flagCreate   = 1;

                        this.clearFields();
                    }

                    if (res['status'] == 'ERROR' && res['code'] == 400)
                    {
                        this.errorMessage = res['message'];
                        this.errorStatus  = res['status'];
                        this.errorCode    = res['code'];
                        this.flagErrorsM  = true;
                    }
                });
    }

    /**
     * Method will be used to clear fields of the form when the user
     * wants to reset all the fields
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    clearFields(): void {
        if (this.flagCreate == 0)
        {
            this.flagSuccessM = false;
        }
        this.flagErrorsM  = false;
        this.submitedForm = false;

        this.errorCode    = 0;
        this.errorMessage = '';
        this.errorStatus  = '';

        this.formGroupAdd.reset();
        this.formGroupAdd.controls.section.setValue('Banners');
        this.formGroupAdd.controls['urlVideo'].disable();
        this.formGroupAdd.controls['file'].disable();
        this.formGroupAdd.controls.options.setValue('');
        this.formGroupAdd.controls.status.setValue('');
    }

    /**
     * Method for redirect to the list page once the user wants to cancel
     * the functionality of add new record
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    cancelOption(): void {
        this._router.navigate(['/admin/banners/list']);
    }

    /**
     * Method for enabled or disable the file or url
     * depending on the selection done by the user
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    disabledEnabledFields(event: any) {
        if (event.target.value == '1') {
            this.formGroupAdd.controls['file'].enable();
            this.formGroupAdd.controls['urlVideo'].disable();
            this.formGroupAdd.controls.urlVideo.setValue('');
        } else {
            this.formGroupAdd.controls['urlVideo'].enable();
            this.formGroupAdd.controls['file'].disable();
            this.formGroupAdd.controls.file.setValue('');
        }
    }

    /**
     * Method will be used to get the form
     * in a single variable to manipulate in the
     * template
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    get f() {
        return this.formGroupAdd.controls;
    }
}
