import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
    title  : string = "Administrator";
    breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'Administrator', link: '', isLink: false }
    ];

    constructor() { }

    ngOnInit() {
    }

}
