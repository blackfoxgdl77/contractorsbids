import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { IMessage } from 'ng2-semantic-ui';
import { DinamycData } from 'src/app/models/dinamyc-data';

@Component({
    selector: 'app-admin-about-us',
    templateUrl: './admin-about-us.component.html',
    styleUrls: ['./admin-about-us.component.scss']
})
export class AdminAboutUsComponent implements OnInit {

    constructor(private api: ApiService) {
    }

    ngOnInit() { }
}
