import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { ImagesService } from 'src/app/services/images.service';
import { Router, Route } from '@angular/router';
import { IMessage } from 'ng2-semantic-ui';
import { DinamycData } from 'src/app/models/dinamyc-data';

@Component({
    selector: 'app-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
    public title  : string = "How It Works / Create Record";
    public breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'Administrator', link: '/admin/administrator', isLink: true },
        { text: 'How It Works - List', link: '/admin/how-it-works/list', isLink: true },
        { text: 'How It Works - Create', link: '', isLink: false }
    ];

    public formGroupAdd : FormGroup;
    public flagSuccessM : boolean = false;
    public flagErrorsM  : boolean = false;
    public submitedForm : boolean = false;
    public filesData    : File    = null;
    public errorStatus  : string  = '';
    public errorMessage : string  = '';
    public errorCode    : number  = 0;
    public flagCode     : number  = 0;

    public editorConfig = {
        editable: true,
        spellcheck: true,
        height: '15rem',
        minHeight: '5rem',
        placeholder: 'Type a description...',
        translate: 'yes'
    };

    constructor(private api: ApiService, 
                private fb: FormBuilder, 
                private img: ImagesService,
                private _router: Router) { } 

    ngOnInit() {
        this.validations();
    }

    /**
     * Method will be used to validate
     * the data of the forms and create a new
     * entrance
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    validations(): void {
        this.formGroupAdd = this.fb.group({
            section     : ['How It Works'],
            description : ['', [Validators.required]],
            options     : ['', [Validators.required]],
            status      : ['', [Validators.required]],
            urlVideo    : [''],
            file        : ['']
        });

        this.formGroupAdd.controls['file'].disable();
        this.formGroupAdd.controls['urlVideo'].disable();
        this.formGroupAdd.controls['section'].disable();
    }

    /**
     * Event will be used to get the information of the 
     * image will be uploaded to the server
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    onFilesLoad(event: any) {
        this.filesData = <File>event.target.files;
    }

    /**
     * Method will be used to save the new record related to about us
     * and will be stores on admin table
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    newRecordCreated(): void {
        let values = this.formGroupAdd.value;
        this.submitedForm = true;
        Object.assign(values, { section: 'How It Works' });

        if (values['options'] == '1') {
            Object.assign(values, { urlVideo: '' });
        }

        if (this.formGroupAdd.invalid) {
            this.flagErrorsM = (this.flagErrorsM) ? this.flagErrorsM : !this.flagErrorsM;

            return ;
        }

        this.flagErrorsM = false;
        this.api.postRequest('/api/admin/create', JSON.stringify(values))
                .subscribe(res => {
                    if (res.body['status'] == 'OK' && res.body['code'] == 200)
                    {
                        let returnedData = res.body['data'];

                        localStorage.setItem('userT1', res.headers.get('_token_sec'));
                        localStorage.setItem('userT2', res.headers.get('_token_log'));
                        localStorage.setItem('number', res.headers.get('_token_user'));

                        if (returnedData['options'] == '1') 
                        {
                            this.uploadImage(returnedData['id']);
                        }
                        else
                        {
                            this.flagSuccessM = true;
                            this.flagCode     = 1;
                            this.clearFields();
                        }
                    }

                    if (res.body['status'] == 'ERROR' && res.body['code'] == 400)
                    {
                        this.errorMessage = res.body['message'];
                        this.errorStatus  = res.body['status'];
                        this.errorCode    = res.body['code'];
                        this.flagErrorsM  = true;
                    }
                });
    }

    /**
     * Method will be disabled once the user clicks on the cross
     * for clear the messages
     * 
     * @param message IMessage
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorbids
     */
    public dismiss(message: IMessage)
    {
        this.flagCode = 0;
        this.clearFields();
    }

    /**
     * Method will be used for upload image in every
     * section of the admin module
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    uploadImage(id): void {
        const formData = new FormData();
        formData.append('file', this.filesData[0]);
        this.img.postImage(`/api/upload/2/${id}/images`, formData)
                .subscribe(res => {
                    if (res.body['status'] == 'OK' && res.body['code'] == 200)
                    {
                        this.flagSuccessM = true;
                        this.flagCode     = 1;
                        this.clearFields();
                    }

                    if (res.body['status'] == 'ERROR' && res.body['code'] == 400)
                    {
                        this.errorMessage = res.body['message'];
                        this.errorStatus  = res.body['status'];
                        this.errorCode    = res.body['code'];
                        this.flagErrorsM  = true;
                    }
                });
    }

    /**
     * Method will be used to clear fields of the form
     * that the user filled
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    clearFields() {
        if (this.flagCode != 1)
        {
            this.flagSuccessM = false;
        }
        this.submitedForm = false;
        this.errorMessage = '';
        this.errorStatus  = '';
        this.errorCode    = 0;
        this.flagErrorsM  = false;

        this.formGroupAdd.reset();
        this.formGroupAdd.controls['file'].disable();
        this.formGroupAdd.controls['urlVideo'].disable();
        this.formGroupAdd.controls.section.setValue('How It Works');
        this.formGroupAdd.controls.options.setValue('');
        this.formGroupAdd.controls.status.setValue('');
    }

    /**
     * Method will disable / enable the fields that will be
     * selected by the user depends on the option choiced
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    disabledEnabledFields(event: any) {
        if (event.target.value == '1') {
            this.formGroupAdd.controls['file'].enable();
            this.formGroupAdd.controls['urlVideo'].disable();
            this.formGroupAdd.controls.urlVideo.setValue('');
        } else {
            this.formGroupAdd.controls['file'].disable();
            this.formGroupAdd.controls['urlVideo'].enable();
            this.formGroupAdd.controls.file.setValue('');
        }
    }

    /**
     * Method will return the user to the list in case cancel
     * the action to create a new record
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    returnBack() {
        return this._router.navigate(['/admin/how-it-works/list']);
    }

    /**
     * Get the form object to manipulate the information
     * in the form
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    get f() {
        return this.formGroupAdd.controls;
    }
}
