import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarningUsersComponent } from './warning-users.component';

describe('WarningUsersComponent', () => {
  let component: WarningUsersComponent;
  let fixture: ComponentFixture<WarningUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarningUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarningUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
