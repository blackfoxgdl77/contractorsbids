import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { IMessage } from 'ng2-semantic-ui';
import { Post } from 'src/app/models/post';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
    flagDeleteRecord  : boolean = false;
    flagSuccessRecord : boolean = false;
    postsList          : Post[];
    messageStatus     : string;
    messageResponse   : string;
    totalRecords      : number;

    constructor(private api: ApiService) { } 

    ngOnInit() {
        this.api.getRequest("/api/posts/search")
                .subscribe(res => {
                    this.postsList  = res['data'];
                    this.totalRecords = this.postsList.length;
                });
    }

    /**
     * Soft-delete the information selected by the user once clicks
     * the delete button
     * 
     * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     * 
     * @params integer id
     */
    /*deleteRecord(id) : void {
        this.api.deleteRequest(`/api/posts/delete/${id}`)
                .subscribe(res => {
                if (res['code'] == 200 && res['status'] == 'OK') {
                    let data = res['data'];
                    this.flagSuccessRecord = true;
                    this.messageStatus     = res['status'];
                    this.messageResponse   = data['message'];
                }

                if (res['code'] == 400 && res['status'] == 'ERROR') {
                    this.flagDeleteRecord = true;
                    this.messageStatus    = res['status'];
                    this.messageResponse  = res['message'];
                }
                })
    }*/

    /**
     * Method will be used to dismiss the message displayed in the
     * banner once the user executes an actions such as delete records
     * 
     * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    /*public dismiss(message: IMessage) {
        this.flagDeleteRecord  = false;
        this.flagSuccessRecord = false;
    }*/
}
