import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WinnerCommentsComponent } from './winner-comments.component';

describe('WinnerCommentsComponent', () => {
  let component: WinnerCommentsComponent;
  let fixture: ComponentFixture<WinnerCommentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WinnerCommentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WinnerCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
