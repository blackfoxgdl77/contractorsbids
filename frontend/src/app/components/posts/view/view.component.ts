import { Component, OnInit } from '@angular/core';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { IMessage } from 'ng2-semantic-ui';
import { AuthService } from 'src/app/services/auth.service';
import { IPayPalConfig, ICreateOrderRequest, IPlatformFee } from 'ngx-paypal';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
    paramId         : any;
    codeResponse    : number;
    statusResponse  : string  = '';
    messageResponse : string  = '';
    headerResponse  : string  = '';
    flagSuccess     : boolean;
    flagError       : boolean;
    makeOffer       : boolean;
    amountOffer     : number;
    authentication  : any;

    postId          : number;
    postName        : string = '';
    postDescription : string = '';
    postPayments    : number;
    categoryPost    : string = '';
    cityPost        : string = '';
    country         : string = '';
    username        : string = '';
    owner           : string = '';
    status          : number;
    userSec         : string = '';
    userLogin       : string = '';
    userN           : string = '';
    createdAt       : string = '';
    // TODO: removes the images and filling in on the get request
    elements        : any = [
        { image: "http://localhost:5000/static/img_admin/banners_sliders/e49fda4b3369f071d50fb92e560ccda9.png", description: "Banner 1", url: "", status: 1, type_file: 1 },
        { image: "http://localhost:5000/static/img_admin/banners_sliders/9540246c9db90dbf553abc760cfb795f.jpeg", description: "Banner 2", url: "", status: 1, type_file: 1 }
    ];

    currentPayment  : number;
    currentUserPay  : string;

    title  : string = "Projects / View";
    breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'Project Dashboard', link: '/admin', isLink: true },
        { text: 'Projects / View', link: '', isLink: false },
    ];

    steps : Object[] = [
        { title: 'Review the Project', icons: 'eye', description: 'Review the Project published' },
        { title: 'Make an Offer', icons: 'money bill alternate outline', description: 'Make an offer to gain the project'},
        { title: 'Get it done!', icons: 'check circle outline', description: 'Work the project if you offer has been selected!' }
    ];

    constructor(private _router: Router,
                private activateRoute: ActivatedRoute,
                private api: ApiService,
                private auth: AuthService) { }

    ngOnInit() {
        (<any>$('.ui.sticky')).sticky();
        (<any>$(".menu .item")).tab();
        this.paramId = this.activateRoute.snapshot.paramMap.get('id');
        this.authentication = this.auth.loggedIn;

        // get post status
        this.api.getRequest(`/api/posts/${this.paramId}/get`)
                .subscribe((res: any) => {
                    // TODO: Implementing observables
                    if (res['body'].code == 200 && res['body'].status == 'OK')
                    {
                        let returnData = res['body'].data;

                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.userSec         = this.auth.userT1;
                        this.userLogin       = this.auth.userT2; 
                        this.userN           = this.auth.number; 
                        this.postId          = returnData.id;
                        this.postName        = returnData.name;
                        this.postDescription = returnData.description;
                        this.postPayments    = returnData.payment;
                        this.categoryPost    = returnData.category;
                        this.cityPost        = returnData.city;
                        this.country         = returnData.country;
                        this.username        = returnData.username;
                        this.owner           = returnData.owner;
                        this.status          = returnData.status;
                        this.createdAt       = returnData.created_at;

                        // here should be filled in the images / videos for displaying on slider
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR')
                    {
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.flagError       = true;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Get the values returned by the child component for execute the 
     * actions required depens on the results
     * 
     * @param object event
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    getMessagesComponent(event) {
        // set variables in the component
        this.messageResponse = event.messageResponse;
        this.codeResponse    = event.codeResponse;
        this.headerResponse  = event.headerResponse;
        this.statusResponse  = event.statusResponse;
        this.flagSuccess     = event.flagSuccess;
        this.flagError       = event.flagError;
    }

    /**
     * Method will be used to dismiss the banners with the messages
     * are going to be displayed in case the message displayed is 
     * an error
     * 
     * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public dismiss(message: IMessage) {
        this.flagError   = false;
        this.flagSuccess = false;
        this.makeOffer   = !this.makeAnOffer;
        this.amountOffer = null;
    }

    /**
     * Method will be used to report the job as fake if the user
     * thinks is fake the project published on the platform
     * 
     * @param id number
     * @param userId string
     * @param userLogin string
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    reportJob(id: number, userN: string, userSec: string, userLogin: string): void {
        let postData = { 'userS' : userSec,
                         'userL' : userLogin,
                         'userN' : userN,
                         'log'   : this.auth.historyLogin };

        this.api.postRequest(`/api/fake/${id}/report`, JSON.stringify(postData))
                .subscribe((res: any) => {
                    // TODO: Implementing observables
                    if (res['body'].code == 200 && res['body'].status == 'OK')
                    {
                        let returnedData = res['body'].data;

                        localStorage.setItem('userT1', returnedData.s_token.userS);
                        localStorage.setItem('userT1', returnedData.s_token.userL);
                        localStorage.setItem('number', returnedData.s_token.userN);

                        this.postId          = returnedData.post;
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.headerResponse  = res['body'].header;
                        this.flagSuccess     = true;
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR')
                    {
                        this.flagError       = true;
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Method used to display the field an give the user the opportunity
     * to make an offer to gain the project
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    makeAnOffer(): void {
        this.makeOffer   = !this.makeOffer;
        this.amountOffer = null;
    }

    /**
     * Method will be used to send the offer to the user and 
     * publish the new offer in the project
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    sendOffer(): boolean {
        if (+this.amountOffer == 0 || +this.amountOffer == null) {
            return false;
        }

        let values = { 'postId' : this.postId,
                       'offer'  : +this.amountOffer };


        this.api.postRequest(`/api/bids/make/offer`, JSON.stringify(values))
                .subscribe(res => {
                    if (res.body['code'] == 200 && res.body['status'] == 'OK')
                    {
                        let dataReturned = res.body['data'];

                        localStorage.setItem('userT1', res.headers.get('_token_sec'));
                        localStorage.setItem('userT2', res.headers.get('_token_log'));
                        localStorage.setItem('number', res.headers.get('_token_user'));

                        this.currentPayment  = dataReturned['money_offer'];
                        this.currentUserPay  = dataReturned['bidder_id'];
                        this.codeResponse    = res.body['code'];
                        this.statusResponse  = res.body['status'];
                        this.messageResponse = res.body['message'];
                        this.headerResponse  = res.body['header'];
                        this.flagSuccess     = true;
                    }

                    if (res.body['code'] == 400 && res.body['status'] == 'ERROR')
                    {
                        this.codeResponse    = res.body['code'];
                        this.statusResponse  = res.body['status'];
                        this.messageResponse = res.body['message'];
                        this.flagError       = true;
                    }
                });

        return true;
    }

    /**
     * Method will be used to check if the user logged in
     * is able to post a comment or make a bid
     * 
     * @return boolean
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    checkIsNotOwner(): boolean {
        if (this.status === 1 && this.auth.username !== this.username) {
            return true;
        }

        return false;
    }

    /**
     * Will be used to activate the bid comments tab once
     * the project has been closed / won
     * 
     * @return boolean
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    activateBidComment(): boolean {
        return false;
    }

    /**
     * Will be used to activate the winner comments form once
     * the project has been closed / won
     * 
     * @return boolean
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    activateWinnerComment(): boolean {
        return false;
    }
}
