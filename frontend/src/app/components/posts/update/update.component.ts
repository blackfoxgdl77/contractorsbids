import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { IMessage } from 'ng2-semantic-ui';
import { Post } from 'src/app/models/post';

@Component({
    selector: 'app-update',
    templateUrl: './update.component.html',
    styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {
    formGroupE   : FormGroup;
    editValues   : [];
    paramId      : any;
    submitedForm : boolean = false;
    flagError    : boolean = false;
    flagSuccess  : boolean = false;

    messages    : string = '';
    statusResp  : string = '';
    codeResp    : number = 0;
    errorMsg    : string = '';

    constructor(private api: ApiService, 
                private fb: FormBuilder, 
                private activatedRoute: ActivatedRoute,
                private _router : Router) { }

    ngOnInit() {
        this.paramId = this.activatedRoute.snapshot.paramMap.get('id');
        this.api.getRequest(`/api/posts/${this.paramId}`)
                .subscribe(res => {
                    if (res.body['code'] == 200 && res.body['status'] == 'OK') {
                        this.editValues = res.body['data'][0];
                        this.statusResp = res.body['status'];
                        this.codeResp   = res.body['code'];
                        this.errorMsg   = res.body['message'];

                        if (parseInt(this.editValues['type_file']) == 1) {
                            this.formGroupE.controls.urlVideo.disable();
                        } else {
                            this.formGroupE.controls.image.disable();
                        }

                        this.fillDataValues();
                    }

                    if (res.body['code'] == 400 && res.body['status'] == 'ERROR') {
                        this.statusResp = res.body['status'];
                        this.codeResp   = res.body['code'];
                        this.errorMsg   = res.body['message'];
                    }
                });

        this.validations();
    }

    /**
     * Method for validate if the fields are filled as expected
     * at the moment to edit records
     * 
     * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    validations(): void {
        this.formGroupE = this.fb.group({
            publishedBy : ['', [Validators.required]],
            postname    : ['', [Validators.required]],
            description : ['', [Validators.required]],
            payment     : ['', [Validators.required]],
            city        : ['', [Validators.required]],
            category    : ['', [Validators.required]],
            options     : ['', [Validators.required]],
            status      : ['', [Validators.required]],
            urlVideo    : [''],
            image       : ['']
        });

    }

    /**
     * Method will be used to assign variables and will be
     * used to display the data on the form before to update the
     * record selected by the user
     * 
     * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    fillDataValues(): void {
        if (this.statusResp === 'OK' && this.codeResp === 200) {
        this.formGroupE.patchValue({
            publishedBy : this.editValues['published_by'],
            postname    : this.editValues['post_name'],
            description : this.editValues['description'],
            payment     : this.editValues['payment'],
            city        : this.editValues['city'],
            category    : this.editValues['category'],
            urlVideo    : this.editValues['url'],
            options     : this.editValues['type_file'],
            status      : this.editValues['status']
        });
        } else {
        this.messages = this.statusResp + '. ' + this.errorMsg;
        }
    }

    /**
     * Method to update an record that exists in the database, so won't
     * create a new records
     * 
     * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    updateRecord(): void {
        let values = this.formGroupE.value;
        this.submitedForm = true;

        if (this.formGroupE.invalid) {
            this.flagError = (this.flagError) ? this.flagError : !this.flagError;

            return ;
        }

        this.flagError   = false;
        this.api.updateRequest(`/api/posts/update/${this.paramId}`, JSON.stringify(values))
                .subscribe(res => {
                    if (res.body['code'] == 200 && res.body['status'] == 'OK') {
                        let dataReturned = res.body['data'];
                        this.codeResp    = res.body['code'];
                        this.statusResp  = res.body['status'];
                        this.errorMsg    = dataReturned['message'];
                        this.flagSuccess = true;
                    }

                    if (res.body['code'] == 400 && res.body['status'] == 'ERROR') {
                        this.flagError  = true;
                        this.codeResp   = res.body['code'];
                        this.statusResp = res.body['status'];
                        this.errorMsg   = res.body['message'];
                    }
                });
    }

    /**
     * Method will cancel the actions and the user
     * will be redirected to the main view, where the users
     * will see the list of all the records
     * 
     * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    cancelOptions() {
        return this._router.navigate(['/posts/dashboard']);
    }

    /**
     * Method will be used to disable or enable the fields
     * depending on the option selected on edit view
     * 
     * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    disabledEnabledFields(event: any) {
        if (event.target.value == '1') {
            this.formGroupE.controls['image'].enable();
            this.formGroupE.controls['urlVideo'].disable();
            this.formGroupE.controls.urlVideo.setValue('');
        } else {
            this.formGroupE.controls['image'].disable();
            this.formGroupE.controls['urlVideo'].enable();
            this.formGroupE.controls.image.setValue('');
        }
    }

    /**
     * Method will return the object could we use
     * for handling the form object in the template
     * 
     * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    get f() {
        return this.formGroupE.controls;
    }

    /**
     * Method will close the modal with sucess message once
     * the user has updated a record
     * 
     * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    dismiss(message : IMessage) {
        this.flagSuccess = false;
    }
}
