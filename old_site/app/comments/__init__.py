from flask import Blueprint

comments = Blueprint("comments", __name__, template_folder="templates")

from app.comments import routes
