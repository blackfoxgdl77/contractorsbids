import os
from flask import Flask, request
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager

db      = SQLAlchemy()
migrate = Migrate()
login   = LoginManager()

def start_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(db);

    # Register the blueprint modules
    from app.website import website
    from app.users import users
    from app.api import api

    app.register_blueprint(website)
    app.register_blueprint(users, url_prefix="/users")
    app.register_blueprint(api, url_prefix="/api")

    return app
