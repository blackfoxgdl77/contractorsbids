from app import db
import os

class User(db.Model):
    id       = db.Column(db.Integer, primary_key=True, index=True)
    username = db.Column(db.String(), index=True, unique=True)
    email    = db.Column(db.String(), index=True, unique=True)
    password = db.Column(db.String())
    
