from flask import Blueprint

admins = Blueprint('admins', __name__, template="templates")

from app.admins import routes
