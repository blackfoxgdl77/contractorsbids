from flask import render_template, request, url_for, g, jsonify
from app.website import website

@website.route('/', methods=['GET'])
@website.route('/index', methods=['GET'])
def index():
    return render_template("index.html")

@website.route('/contact', methods=['GET'])
def contact():
    return render_template("contact.html")

@website.route('/how-it-works', methods=['GET'])
def howItWorks():
    return render_template('works.html')

@website.route('/about-us', methods=['GET'])
def aboutUs():
    return render_template('about.html')

@website.route('/terms-conditions', methods=['GET'])
def termsAndConditions():
    return render_template('termsConditions.html')
