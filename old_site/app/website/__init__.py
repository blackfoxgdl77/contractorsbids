from flask import Blueprint

website = Blueprint('website', __name__, template_folder="templates")

from app.website import routes
