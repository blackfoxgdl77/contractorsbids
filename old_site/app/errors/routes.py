from flask import abort, request, render_template
from app.api import errors

@errors.app_errorhandlers(404)
def not_found_error():
    pass
