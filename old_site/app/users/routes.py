from flask import render_template, jsonify, request, g, url_for
from flask_login import login_user, logout_user, login_required
from app.users import users

@users.route('/signin', methods=['GET'])
def signin():
    return render_template('signin.html')

@users.route('/signup', methods=['GET'])
def signup():
    return render_template('signup.html')

@users.route('/logout')
def logout():
    logout_user()

    return redirect(url_for('website.index'))
