"""winner comments and publish comments new table - 18-JULIO-2019

Revision ID: 3f61e7568f5c
Revises: 7c3b319bd5b5
Create Date: 2019-07-18 00:38:56.286750

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '3f61e7568f5c'
down_revision = '7c3b319bd5b5'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('publish_winner',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('title', sa.String(length=200), nullable=False),
    sa.Column('comment', sa.Text(), nullable=True),
    sa.Column('status', sa.Boolean(), nullable=True),
    sa.Column('project_winner', sa.Integer(), nullable=True),
    sa.Column('project_id', sa.Integer(), nullable=True),
    sa.Column('date_created', sa.DateTime(), nullable=True),
    sa.Column('date_updated', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['project_id'], ['post.id'], ),
    sa.ForeignKeyConstraint(['project_winner'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_publish_winner_id'), 'publish_winner', ['id'], unique=False)
    op.create_table('comment_winner',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('post_id', sa.Integer(), nullable=True),
    sa.Column('comments', sa.Text(), nullable=True),
    sa.Column('status', sa.Boolean(), nullable=True),
    sa.Column('publish_id', sa.Integer(), nullable=True),
    sa.Column('date_created', sa.DateTime(), nullable=True),
    sa.Column('date_updated', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['post_id'], ['post.id'], ),
    sa.ForeignKeyConstraint(['publish_id'], ['publish.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_comment_winner_id'), 'comment_winner', ['id'], unique=False)
    op.alter_column('publish', 'title',
               existing_type=mysql.VARCHAR(length=200),
               nullable=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('publish', 'title',
               existing_type=mysql.VARCHAR(length=200),
               nullable=True)
    op.drop_index(op.f('ix_comment_winner_id'), table_name='comment_winner')
    op.drop_table('comment_winner')
    op.drop_index(op.f('ix_publish_winner_id'), table_name='publish_winner')
    op.drop_table('publish_winner')
    # ### end Alembic commands ###
