"""v7

Revision ID: 85075cd594de
Revises: 336f8733ffe5
Create Date: 2019-06-21 01:08:34.266288

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '85075cd594de'
down_revision = '336f8733ffe5'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint('bid_ibfk_2', 'bid', type_='foreignkey')
    op.drop_column('bid', 'bid_owner')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('bid', sa.Column('bid_owner', mysql.INTEGER(display_width=11), autoincrement=False, nullable=True))
    op.create_foreign_key('bid_ibfk_2', 'bid', 'user', ['bid_owner'], ['id'])
    # ### end Alembic commands ###
