"""removes auctioner, must be get from post table, 20-jun

Revision ID: 7e6ad214b8e7
Revises: b6d09050cbaa
Create Date: 2019-06-20 23:35:11.286240

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '7e6ad214b8e7'
down_revision = 'b6d09050cbaa'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint('offer_ibfk_1', 'offer', type_='foreignkey')
    op.drop_column('offer', 'auctioner_user')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('offer', sa.Column('auctioner_user', mysql.INTEGER(display_width=11), autoincrement=False, nullable=True))
    op.create_foreign_key('offer_ibfk_1', 'offer', 'user', ['auctioner_user'], ['id'])
    # ### end Alembic commands ###
