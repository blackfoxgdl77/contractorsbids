from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS, cross_origin
from flask_jwt_extended import JWTManager
from flask_mail import Mail
import requests
import os

db      = SQLAlchemy();
migrate = Migrate();

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    CORS(app, resources={r"/api/*": {"origins": "*"}}, expose_headers=['_token_adm', '_token_sec', '_token_log', '_token_user'])
    db.init_app(app)
    migrate.init_app(app, db)
    jwt = JWTManager(app)
    mail = Mail(app)

    from app.admin import admin
    from app.websites import websites
    from app.bids import bids
    from app.categories import categories
    from app.cities import cities
    from app.comments import comments
    from app.payments import payments
    from app.posts import posts
    from app.users import users
    from app.fakes import fakes
    from app.imports import imports

    app.register_blueprint(admin)
    app.register_blueprint(websites)
    app.register_blueprint(bids)
    app.register_blueprint(categories)
    app.register_blueprint(cities)
    app.register_blueprint(comments)
    app.register_blueprint(payments)
    app.register_blueprint(posts)
    app.register_blueprint(users)
    app.register_blueprint(imports)
    app.register_blueprint(fakes)

    blacklist = set()

    return app

from app import models
