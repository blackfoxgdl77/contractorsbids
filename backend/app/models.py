from app import db
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime, timedelta
from time import time
import hashlib, uuid
import json, os
from flask import jsonify, current_app as app

class User(db.Model):
    id        = db.Column(db.Integer, primary_key=True)
    name      = db.Column(db.String(150), index=True)
    email     = db.Column(db.String(300), index=True, unique=True)
    username  = db.Column(db.String(200), index=True, unique=True)
    password  = db.Column(db.String(200), index=True, unique=True)
    is_company = db.Column(db.Boolean, default=False)
    type_user  = db.Column(db.Integer)
    is_active  = db.Column(db.Integer)
    is_admin   = db.Column(db.Boolean, default=False)
    activation_token = db.Column(db.String(200))
    is_confirm_token = db.Column(db.Boolean, default=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, default=datetime.utcnow)
    email_base = db.Column(db.String(300), index=True, unique=True)

    #relationships
    posts = db.relationship('Post', backref='user_post', lazy="dynamic")
    fakes = db.relationship('ReportFake', backref="user_fake", lazy="dynamic")
    usersinfo = db.relationship('UserInfo', backref="info", lazy='dynamic')
    userresetpwd = db.relationship('UserResetPassword', backref="resetpwd", lazy="dynamic")
    loginhistory = db.relationship('LoginHistory', backref="historylogin", lazy="dynamic")
    loghistory = db.relationship('ErrorLog', backref="usererrorlog", lazy="dynamic")
    sectoken = db.relationship('SecurityToken', backref="sec_token", lazy="dynamic")
    userbid = db.relationship('Bid', backref="user_bid", lazy="dynamic")
    userpublish = db.relationship('Publish', backref="user_publish", lazy="dynamic")
    usercomment = db.relationship('Comment', backref="user_comment", lazy="dynamic")
    userpayment = db.relationship('PaymentHistory', backref="payment_user", lazy="dynamic")
    userwinnerp = db.relationship('PublishWinner', backref="user_winner_pub", lazy="dynamic")
    userwinnerc = db.relationship('CommentWinner', backref="user_winner_com", lazy="dynamic")

    def encrypt_user_number(self, user_id, login_date):
        userid = "{}{}{}".format(user_id, login_date, uuid.uuid4())
        string_encrypt = hashlib.md5(userid.encode())

        return string_encrypt.hexdigest()

    def generate_password(self, text):
        return generate_password_hash(text)

    def check_password(self, pwd):
        return check_password_hash(self.password, pwd)

    def generate_username_dynamic(self, id):
        str_id = str(id)
        fill_zeros   = 0
        new_username = None

        if len(str_id) <= 5:
            fill_zeros = 6 - len(str_id)

        new_username = "CB_" + str_id.zfill(fill_zeros)

        return new_username

class Country(db.Model):
    id = db.Column(db.Integer, primary_key=True, index=True)
    name_country = db.Column(db.String(80), index=True, nullable=False)
    country_code = db.Column(db.String(10), nullable=False)
    status_city  = db.Column(db.Boolean, default=True)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

    # relationship
    countries = db.relationship('City', backref="country_city", lazy="dynamic")

class City(db.Model):
    id = db.Column(db.Integer, primary_key=True, index=True)
    city_name = db.Column(db.String(80), index=True) #city field
    country = db.Column(db.String(80), index=True) #country field
    capital = db.Column(db.String(80)) #admin_name field
    code = db.Column(db.String(5)) #iso3
    unique_id = db.Column(db.String(10)) #id unique_id field
    latitude = db.Column(db.Numeric(precision=9, scale=6))
    longitude = db.Column(db.Numeric(precision=9, scale=6))
    country_id = db.Column(db.Integer, db.ForeignKey('country.id'))
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

    # relationship
    posts = db.relationship('Post', backref='post_cities', lazy="dynamic")

class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True, index=True)
    name = db.Column(db.String(100), index=True)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

    # relationship
    posts = db.relationship('Post', backref="post_category", lazy="dynamic")

class UserInfo(db.Model): #done
    id = db.Column(db.Integer, primary_key=True, index=True)
    phone = db.Column(db.String(50), nullable=True)
    payment_method = db.Column(db.String(100), nullable=True)
    bbb_account = db.Column(db.String(200), nullable=True)
    bbb_accredited = db.Column(db.String(200), nullable=True)
    raiting = db.Column(db.String(10), nullable=True)
    wcb_number = db.Column(db.String(100), nullable=True)
    address = db.Column(db.String(255), nullable=True)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, default=datetime.utcnow)

    # relationship
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

class UserResetPassword(db.Model): #done
    id = db.Column(db.Integer, primary_key=True, index=True)
    token_reset = db.Column(db.String(255))
    is_active = db.Column(db.Boolean, default=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user_email = db.Column(db.String(200), nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, default=datetime.utcnow)

    def set_password_reset(self, email, date):
        token = "{}{}{}".format(date, email, date)
        return generate_password_hash(token)

class Post(db.Model): #done
    id = db.Column(db.Integer, primary_key=True, index=True)
    post_name = db.Column(db.String(150))
    description = db.Column(db.Text)
    post_payment = db.Column(db.Float)
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
    city_id = db.Column(db.Integer, db.ForeignKey('city.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    post_status = db.Column(db.Integer) # 1-active, 0-won, 2-soft-deleted, 3-payed
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, default=datetime.utcnow)

    #relationships
    attachments = db.relationship('Attachment', backref="post_attachment", lazy="dynamic")
    fakes = db.relationship('ReportFake', backref="post_fake", lazy="dynamic")
    bids = db.relationship('Bid', backref="post_bid", lazy="dynamic")
    publish = db.relationship('Publish', backref="post_publish", lazy="dynamic")
    comments = db.relationship('Comment', backref="post_comment", lazy="dynamic")
    payments = db.relationship('PaymentHistory', backref="post_payment", lazy="dynamic")
    winnerp = db.relationship('PublishWinner', backref="post_winner_publish", lazy="dynamic")
    winnerc = db.relationship('CommentWinner', backref="post_winner_comment", lazy="dynamic")

class DynamicData(db.Model): #done
    id = db.Column(db.Integer, primary_key=True, index=True)
    section = db.Column(db.Integer, nullable=True, index=True)
    description = db.Column(db.Text, nullable=True)
    file = db.Column(db.String(500), nullable=True)
    file_size = db.Column(db.String(500), nullable=True)
    file_slider = db.Column(db.String(500), nullable=True)
    original_name = db.Column(db.String(500), nullable=True)
    url = db.Column(db.String(500), nullable=True)
    type_file = db.Column(db.Integer, nullable=True)
    file_extension = db.Column(db.String(10), nullable=True)
    status = db.Column(db.Integer, index=True)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, default=datetime.utcnow)

    def to_dict(self):
        data = [{
            'id'           : self.id,
            'section'      : self.section,
            'description'  : self.description,
            'date_created' : self.date_created,
            'status'       : self.status,
            'type_file'    : self.type_file
        }]

        if self.url != None:
            data['url'] = self.url

        if self.file != None:
            data['file'] = {
                'file_extension' : self.file_extension,
                'file_size'      : self.file_size,
                'original_name'  : self.original_name,
                'file'           : self.file
            }

        return data

    def get_value_string_status(self, stus):
        value = "Active" if stus == 1 else "Inactive"

        return value

    def get_objects(self):
        data = {
            'id': self.id,
            'section': self.section,
            'description': self.description
        }

        return jsonify(data)

class Attachment(db.Model): #done
    id             = db.Column(db.Integer, primary_key=True, index=True)
    name           = db.Column(db.String(200))
    path           = db.Column(db.String(100))
    is_image       = db.Column(db.Integer)
    file_extension = db.Column(db.String(30), nullable=True)
    post_id        = db.Column(db.Integer, db.ForeignKey('post.id'))
    date_created   = db.Column(db.DateTime, default=datetime.utcnow)
    date_updated   = db.Column(db.DateTime, default=datetime.utcnow)

class LoginHistory(db.Model):
    id           = db.Column(db.Integer, primary_key=True, index=True)
    token        = db.Column(db.Text, nullable=False)
    token_sec    = db.Column(db.Text, nullable=False)
    token_login  = db.Column(db.Text, nullable=False)
    status       = db.Column(db.Boolean, default=True)
    user_id      = db.Column(db.Integer, db.ForeignKey('user.id'))
    date_login   = db.Column(db.DateTime, default=datetime.utcnow)
    update_login = db.Column(db.DateTime, default=datetime.utcnow)

class ReportFake(db.Model):
    id = db.Column(db.Integer, primary_key=True, index=True)
    fake_project_id = db.Column(db.Integer, db.ForeignKey('post.id'))
    reported_user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    status_job = db.Column(db.Integer)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, default=datetime.utcnow)

    def status_fake_job(self, status):
        status_value = "Active" if status == 1 else "Inactive"

        return status_value

class ErrorLog(db.Model):
    id           = db.Column(db.Integer, primary_key=True, index=True)
    log_message  = db.Column(db.Text, nullable=False)
    error_code   = db.Column(db.String(15), nullable=False)
    module_error = db.Column(db.String(80), nullable=False)
    action_error = db.Column(db.String(80), nullable=False)
    user_login   = db.Column(db.Integer, db.ForeignKey('user.id'))
    date_error   = db.Column(db.DateTime, default=datetime.utcnow)

class Bid(db.Model):
    id           = db.Column(db.Integer, primary_key=True, index=True)
    bid_post     = db.Column(db.Integer, db.ForeignKey('post.id'))
    bid_user     = db.Column(db.Integer, db.ForeignKey('user.id')) # user will do offers
    bid_money    = db.Column(db.Float)
    bid_status   = db.Column(db.Integer, default=1) # 0 - old, 1 - active, 2 - won, 3 - declined
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, default=datetime.utcnow)

class SecurityToken(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    login_sec_token = db.Column(db.Text, nullable=False)
    login_token = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), index=True)
    token_user = db.Column(db.Text, nullable=True)
    status = db.Column(db.Boolean, default=True)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, default=datetime.utcnow)

    def get_security_token(self, email, user_id, name):
        string_sec_token = "{}{}{}{}{}{}{}{}{}{}".format(uuid.uuid4(), email, user_id, name, app.config['SECURITY_TOKEN_CB'], datetime.utcnow, name, email, user_id, uuid.uuid4())
        sec_token = hashlib.md5(string_sec_token.encode())

        return sec_token.hexdigest()

    def get_login_token(self, email, username, user_id):
        string_log_token = "{}{}{}{}{}{}{}{}{}{}".format(uuid.uuid4(), email, username, user_id, app.config['LOGIN_TOKEN_CB'], datetime.utcnow, username, user_id, email, uuid.uuid4())
        log_token = hashlib.md5(string_log_token.encode())

        return log_token.hexdigest()

class Publish(db.Model): # the owner will be get from post table
    id = db.Column(db.Integer, primary_key=True, index=True)
    user_publish_id = db.Column(db.Integer, db.ForeignKey('user.id'), index=True) #user will post the comment
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'), index=True)
    title = db.Column(db.String(200), nullable=False)
    status = db.Column(db.Boolean, default=True)
    comment = db.Column(db.Text)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, default=datetime.utcnow)

    #relationship
    publishcomment = db.relationship('Comment', backref="publish_comment", lazy="dynamic")

    def get_comment_available(self, id, status):
        comments = []
        inner    = Comment().query.filter_by(publish_id=id, status=status).order_by(Comment.date_created.desc()).all()

        if inner is not None:
            comments = [{
                'inner_id' : inner_comments.id,
                'publish'  : inner_comments.publish_id,
                'username' : inner_comments.user_comment.username,
                'postname' : inner_comments.post_comment.post_name,
                'postid'   : inner_comments.post_id,
                'comment'  : inner_comments.comments,
                'created'  : inner_comments.date_created,
                'status'   : 'Active'
            } for inner_comments in inner]

        return comments

class Comment(db.Model): # the owner will be get from post table
    id = db.Column(db.Integer, primary_key=True, index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), index=True) #user will post the comment
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'), index=True)
    comments = db.Column(db.Text)
    publish_id = db.Column(db.Integer, db.ForeignKey('publish.id'))
    status = db.Column(db.Boolean, default=True)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    date_updates = db.Column(db.DateTime, default=datetime.utcnow)

class PaymentHistory(db.Model):
    id = db.Column(db.Integer, primary_key=True, index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'))
    amount = db.Column(db.Float)
    order_number = db.Column(db.String(100), nullable=False)
    payment_source = db.Column(db.String(50), nullable=False)
    currency = db.Column(db.String(10), nullable=False)
    status = db.Column(db.String(50), nullable=False)
    transaction_id = db.Column(db.String(150), nullable=True)
    payer_email = db.Column(db.String(150), nullable=True)
    given_name = db.Column(db.String(250), nullable=True)
    surname = db.Column(db.String(250), nullable=True)
    merchant_id = db.Column(db.String(150), nullable=True)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, default=datetime.utcnow)

    def type_of_currency(self, country):
        return "CAD" if country.upper() == 'canada' else "USD"

class PublishWinner(db.Model):
    id = db.Column(db.Integer, primary_key=True, index=True)
    title = db.Column(db.String(200), nullable=False)
    comment = db.Column(db.Text)
    status = db.Column(db.Boolean, default=True)
    project_winner = db.Column(db.Integer, db.ForeignKey('user.id'))
    project_id = db.Column(db.Integer, db.ForeignKey('post.id'))
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, default=datetime.utcnow)

    #relationship
    winner_comments = db.relationship('CommentWinner', backref="winner_comment", lazy="dynamic")

    def get_winner_comment_available(self, id, status):
        winner = []
        inner  = CommentWinner().query.filter_by(publish_id=id, status=status).order_by(CommentWinner.date_created.desc()).all()

        if inner is not None:
            winner = [{
                'inner_id' : inner_winner.id,
                'publish'  : inner_winner.publish_id,
                'username' : inner_winner.user_winner_com.username,
                'postname' : inner_winner.post_winner_comment.post_name,
                'postid'   : inner_winner.post_id,
                'comment'  : inner_winner.comments,
                'created'  : inner_winner.date_created,
                'status'   : 'Active'
            } for inner_winner in inner]

        return winner

class CommentWinner(db.Model):
    id = db.Column(db.Integer, primary_key=True, index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'))
    comments = db.Column(db.Text)
    status = db.Column(db.Boolean, default=True)
    publish_id = db.Column(db.Integer, db.ForeignKey('publish_winner.id'))
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, default=datetime.utcnow)
