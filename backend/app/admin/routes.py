from app.admin import admin
from app.libraries.jwtFunctions import JwtFunctions
from flask import jsonify, json, request, make_response, current_app as app
import requests, os
from app.models import DynamicData, ErrorLog, User, ReportFake, Post, SecurityToken, LoginHistory
from app import db
from datetime import datetime
from flask_jwt_extended import jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt
from werkzeug.utils import secure_filename
from app.libraries.uploadFiles import UploadFiles
from app.libraries.encryptInformation import EncryptInformation
from PIL import Image

@admin.route('/api/admin/<int:id>/get', methods=['GET'])
@jwt_refresh_token_required
def get_data(id):
    try:
        data    = {}
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()
        data      = DynamicData.query.filter_by(id=id)

        if _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif data is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The record was not found. Please contact contractorsbids Support Team' })
        else:
            response = jsonify({ 'code'   : 200,
                                 'status' : 'OK',
                                 'token'  : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'   : [{
                                    'id'             : record.id,
                                    'description'    : record.description,
                                    'section'        : get_admin_value(record.section),
                                    'status'         : record.status,
                                    'type_file'      : record.type_file,
                                    'url'            : record.url,
                                    'file'           : record.file,
                                    'file_size'      : record.file_size,
                                    'file_extension' : record.file_extension,
                                    'date_created'   : record.date_created,
                                    'date_updated'   : record.date_updated,
                                    'original_name'  : record.original_name } for record in data]})

        final_response         = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_security.login_sec_token,
                                                                             _security.login_token,
                                                                             _security.token_user,
                                                                             _history.id)

        return final_response
    except Exception as e:
        print(e)
        error = jsonify({ 'code'   : 400,
                          'status' : 'ERROR',
                          'message': app.config['EXCEPTION_MESSAGE_ERROR'] })

        final_response = make_response(error)
        return final_response

@admin.route('/api/admin/<int:id>/get/section', methods=['GET'])
@jwt_refresh_token_required
def get_data_section(id):
    try:
        data    = {}
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()
        data      = DynamicData.query.filter(DynamicData.section == id, DynamicData.status != 3)

        if _security is None and _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif data is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The data can\'t be recovery. Please contact contractorsbids Support Team.' })
        else:
            response = jsonify({ 'code'   : 200,
                                 'status' : 'OK',
                                 'token'  : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'   : [
                                    {
                                        'id'           : section.id,
                                        'section'      : get_admin_value(section.section),
                                        'description'  : section.description,
                                        'date_created' : section.date_created,
                                        'status'       : section.status,
                                        'file'         : section.file_size,
                                        'url'          : section.url,
                                        'type_file'    : section.type_file
		                     } for section in data ]})

        final_response         = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_security.login_sec_token,
                                                                             _security.login_token,
                                                                             _security.token_user,
                                                                             _history.id)

        return final_response
    except Exception as e:
        print(e)
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })

        final_response = make_response(error)
        return final_response

# method to create new record
@admin.route('/api/admin/create', methods=['POST'])
@jwt_refresh_token_required
def add_new_record():
    try:
        newpost = request.get_json()
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()

        if _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })

        else:
            _token_sec = EncryptInformation().generate_token_sec(_security.sec_token.email, _security.user_id, _security.sec_token.name)
            _token_log = EncryptInformation().generate_token_login(_security.sec_token.email, _security.sec_token.username, _security.user_id)
            _token_num = EncryptInformation().encrypt_user_id(_security.user_id, _history.date_login)

            #update sec token table
            _security.login_sec_token = _token_sec
            _security.login_token     = _token_log
            _security.token_user      = _token_num
            db.session.commit()

            #update login history table
            _history.token_login = _token_log
            _history.token_sec   = _token_sec
            db.session.commit()

            total = DynamicData.query.filter(DynamicData.section == set_admin_value(newpost['section'])).count()

            if (total > 0 and newpost['status'] == '1' and newpost['section'] != 'Banners'):
                updates = DynamicData.query.filter(DynamicData.section == set_admin_value(newpost['section'])).update(dict(status=2))
                db.session.commit()

            admin = DynamicData(section     = set_admin_value(newpost['section']),
                                description = newpost['description'],
                                type_file   = newpost['options'],
                                status      = newpost['status'],
                                url         = newpost['urlVideo'])
            db.session.add(admin)
            db.session.commit()
            db.session.flush()

            response = jsonify({ 'code'   : 200,
                                 'status' : 'OK',
                                 'token'  : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'   : {
                                    'id'      : admin.id,
                                    'section' : newpost['section'],
                                    'options' : newpost['options'],
                                    'message' : 'The record has been saved successfully!'
                                }})

        final_response         = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_security.login_sec_token,
                                                                             _security.login_token,
                                                                             _security.token_user,
                                                                             _history.id)

        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })

        final_response = make_response(error)

        return final_response

# method upload image
@admin.route('/api/upload/<int:section>/<int:id>/images', methods=['POST', 'PUT'])
@jwt_refresh_token_required
def images(section, id):
    try:
        file     = request.files['file']
        filename = secure_filename(file.filename)

        if (UploadFiles().get_extension_file(filename) == False):
            return jsonify({ 'code'    : 400,
                             'status'  : 'ERROR',
                             'message' : 'File incorrect. Please select a valid File.' })

        final_name    = UploadFiles().new_filename(id, filename, section, 0)
        file_ext      = UploadFiles().get_extension_file(filename)
        path_file      = app.config['UPLOAD_BANNERS'] if section == 1 else app.config['UPLOAD_ADMIN']
        new_filename  = "{}.{}".format(final_name, file_ext)
        file.save(os.path.join(path_file, new_filename))

        file_name_resize    = UploadFiles().new_filename(id, filename, section, 1)
        path_file_resize    = app.config['UPLOAD_BANNERS_REZ'] if section == 1 else app.config['UPLOAD_ADMIN_REZ']
        new_filename_resize = "{}.{}".format(file_name_resize, file_ext)
        new_file_resize     = Image.open(os.path.join(path_file, new_filename))

        if new_file_resize.width > new_file_resize.height:
            new_file_resize.thumbnail((1100, 550))
            new_file_resize.save(os.path.join(path_file_resize, new_filename_resize))
        else:
            new_file_resize.thumbnail((550, 1100))
            new_file_resize.save(os.path.join(path_file_resize, new_filename_resize))

        # add new image for slider
        if section == 1:
            file_name_sliders    = UploadFiles().new_filename(id, filename, section, 1)
            path_file_sliders    = app.config['UPLOAD_BANNERS_SLIDERS']
            new_filename_sliders = "{}.{}".format(file_name_sliders, file_ext)
            new_file_sliders     = Image.open(os.path.join(path_file, new_filename))

            new_file_sliders.thumbnail((1100, 550));
            new_file_sliders.save(os.path.join(path_file_sliders, new_filename_sliders))

        # save data in database
        admin = DynamicData.query.filter_by(id=id).first()

        admin.file = "{}{}{}".format(request.url_root, UploadFiles().path_to_save_db(section), new_filename)
        admin.file_size = "{}{}{}".format(request.url_root, UploadFiles().path_to_save_db_resize(section), new_filename_resize)
        admin.original_name  = filename
        admin.file_extension = file_ext
        if section == 1:
            admin.file_slider = "{}{}{}".format(request.url_root, UploadFiles().path_to_save_db_sliders(), new_filename_sliders)
        db.session.commit()

        return jsonify({ 'code'   : 200,
                         'status' : 'OK',
                         'token'  : JwtFunctions().refresh_token(get_jwt_identity()),
                         'data'   : {
                            'id'      : id,
                            'section' : section,
                            'message' : 'Record saved successfully.'
                         }})

    except Exception as e:
        return jsonify({ 'code'    : 400,
                         'status'  : 'ERROR',
                         'message' : app.config['EXCEPTION_MESSAGE_IMAGES'] })

# method to update record
@admin.route('/api/admin/<int:id>/update', methods=['POST', 'PUT'])
@jwt_refresh_token_required
def update_record(id):
    try:
        updates = request.get_json()
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()

        if _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif updates is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'Invalid Data!' })
        else:
            _token_sec = EncryptInformation().generate_token_sec(_security.sec_token.email, _security.user_id, _security.sec_token.name)
            _token_log = EncryptInformation().generate_token_login(_security.sec_token.email, _security.sec_token.username, _security.user_id)
            _token_num = EncryptInformation().encrypt_user_id(_security.user_id, _history.date_login)

            #update sec token table
            _security.login_sec_token = _token_sec
            _security.login_token     = _token_log
            _security.token_user      = _token_num
            db.session.commit()

            #update login history table
            _history.token_login = _token_log
            _history.token_sec   = _token_sec
            db.session.commit()

            if updates['status'] == 1 and updates['section'] != 'Banners':
                update = DynamicData.query.filter(DynamicData.section == set_admin_value(updates['section'])).update(dict(status=2))
                db.session.commit()

            record = DynamicData.query.filter_by(id=id).first()

            record.description  = updates['description']
            record.status       = updates['status']
            record.type_file    = updates['options']
            record.date_updated = datetime.utcnow()
            record.url          = updates['urlVideo']

            db.session.commit()

            response = jsonify({ 'code'   : 200,
                                 'status' : 'OK',
                                 'token'  : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'   : {
                                    'message' : 'Record updated successfully',
                                    'id'      : id
                                }})

        final_response         = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_security.login_sec_token,
                                                                             _security.login_token,
                                                                             _security.token_user,
                                                                             _history.id)

        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        final_response = make_response(error)
        return final_response

# method for delete record
@admin.route('/api/admin/<int:id>/delete', methods=['DELETE', 'GET'])
@jwt_refresh_token_required
def delete_record(id):
    try:
        delete  = DynamicData.query.get(id)
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()

        if _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif delete is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The record you have tried to delete does not exists!' })
        else:
            _token_sec = EncryptInformation().generate_token_sec(_security.sec_token.email, _security.user_id, _security.sec_token.name)
            _token_log = EncryptInformation().generate_token_login(_security.sec_token.email, _security.sec_token.username, _security.user_id)
            _token_num = EncryptInformation().encrypt_user_id(_security.user_id, _history.date_login)

            #update sec token table
            _security.login_sec_token = _token_sec
            _security.login_token     = _token_log
            _security.token_user      = _token_num
            db.session.commit()

            #update login history table
            _history.token_login = _token_log
            _history.token_sec   = _token_sec
            db.session.commit()

            delete.status = 3
            db.session.commit()

            response = jsonify({ 'code'   : 200,
                                 'status' : 'OK',
                                 'token'  : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'   : {
                                        'header'  : 'Record Deleted!',
                                        'message' : 'The record has been deleted successfully',
                                        'id'      : id
                                    }
                                })

        final_response         = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_security.login_sec_token,
                                                                             _security.login_token,
                                                                             _security.token_user,
                                                                             _history.id)

        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        final_response = make_response(error)
        return final_response

#send a warning - Pending
@admin.route('/api/warning/<int:id>/admin', methods=['GET'])
def send_warning(id):
    try:
        user = User.query.filter_by(id=id).first()

        if user is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The user doesn\'t exists in the platform. Please contractorsbids Support Team.' })
        else:
            response = jsonify({ 'code'   : 200,
                                 'status' : 'OK',
                                 'data'   : {
                                    'message'  : '',
                                    'username' : '',
                                    'status'   : '',
                                    'header'   : ''
                                 }})

        return response
    except Exception as e:
        return jsonify({ 'code'    : 400,
                         'status'  : 'ERROR',
                         'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })

# update availability of the admin section - Pending
@admin.route('/api/availability/<int:id>/<int:status>/admin', methods=['DELETE', 'GET'])
@jwt_refresh_token_required
def availability_admin(id, status):
    try:
        admin   = DynamicData.query.filter_by(id=id).first()
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()

        if _security is None and _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif admin is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The record does not exists on the system!' })
        else:
            if admin.section != 1:
                updates = DynamicData.query.filter(DynamicData.section == admin.section, DynamicData.id != admin.id).update(dict(status=2))

            admin.status = status
            db.session.commit()
            db.session.flush()

            message = "The record has been {} successfully.".format(DynamicData().get_value_string_status(status))
            header  = "Record {}".format(DynamicData().get_value_string_status(status))

            data      = DynamicData.query.filter(DynamicData.section == id, DynamicData.status != 3)

            response = jsonify({ 'code'   : 200,
                                 'status' : 'OK',
                                 'token'  : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'   : [
                                    {
                                        'id'           : section.id,
                                        'section'      : get_admin_value(section.section),
                                        'description'  : section.description,
                                        'date_created' : section.date_created,
                                        'status'       : section.status,
                                        'type_file'    : section.type_file
		                     } for section in data ]})

            """response = jsonify({ 'code'   : 200,
                                 'status' : 'OK',
                                 'data'   : {
                                    'message'     : message,
                                    'id'          : admin.id,
                                    'status'      : admin.status,
                                    'description' : admin.description,
                                    'headerMsg'   : header,
                                    'satus_value' : DynamicData().get_value_string_status(status)
                                 }})"""


        final_response         = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_security.login_sec_token,
                                                                             _security.login_token,
                                                                             _security.token_user,
                                                                             _history.id)

        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })

        final_response = make_response(error)
        return final_response

# list jobs reported fake
@admin.route('/api/jobs/reported/fake/all', methods=['GET'])
@jwt_refresh_token_required
def jobs_reported_fake():
    try:
        report  = ReportFake.query.all()
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()

        if _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS']})
        elif report is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'Fake Jobs data invalid!' })
        else:
            response = jsonify({ 'code'   : 200,
                                 'status' : 'OK',
                                 'mesage' : 'There are jobs reported as \'Fake\'',
                                 'token'  : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'   : [
                                    {
                                        'fake_id'          : report.id,
                                        'project_id'       : report.fake_project_id.id,
                                        'project_name'     : report.fake_project_id.post_name,
                                        'category'         : report.fake_project_id.category_id.name,
                                        'city'             : report.fake_project_id.city_id.city_name,
                                        'status'           : report.status_job,
                                        'status_project'   : report.fake_project_id.post_status,
                                        'creator_username' : report.fake_project_id.user_id.username,
                                        'creator_name'     : report.fake_project_id.user_id.name,
                                        'creator_email'    : report.fake_project_id.user_id.email,
                                    } for data in report
                                 ]})

        final_response        = make_response(response)
        final_response.header = EncryptInformation().create_response_header(_security.login_sec_token,
                                                                            _security.login_token,
                                                                            _security.token_user,
                                                                            _history.id)

        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })

        final_response = make_response(error)
        return final_response

# change status fake projects reports
@admin.route('/api/report/<int:id>/<int:id_project>/<int:status>/fake', methods=['GET'])
@jwt_refresh_token_required
def change_status(id, id_project, status):
    try:
        fake    = ReportFake.query.filter_by(id=id_project).first()
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()

        if _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif fake is None:
            message = "The project can\'t be {}. Please verify if the project has been reported as fake".format(ReportFake().status_fake_job(status))
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : message })
        else:
            _token_sec = EncryptInformation().generate_token_sec(_security.sec_token.email, _security.user_id, _security.sec_token.name)
            _token_log = EncryptInformation().generate_token_login(_security.sec_token.email, _security.sec_token.username, _security.user_id)
            _token_num = EncryptInformation().encrypt_user_id(_security.user_id, _history.date_login)

            #update sec token table
            _security.login_sec_token = _token_sec
            _security.login_token     = _token_log
            _security.token_user      = _token_num
            db.session.commit()

            #update login history table
            _history.token_login = _token_log
            _history.token_sec   = _token_sec
            db.session.commit()

            project = Post.query.filter_by().first()

            project.post_status = status
            fake.status_job     = status

            db.session.commit()
            db.session.flush()

            message  = "The Job Reported as 'Fake' as been {}".format(ReportFake().status_fake_job(status))
            header   = "Job Reported {}".format(ReportFake().status_fake_job(status))
            response = jsonify({ 'code'   : 200,
                                 'status' : 'OK',
                                 'token'  : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'   : {
                                    'message'    : message,
                                    'header'     : header,
                                    'fake_id'    : id,
                                    'project_id' : id_project
                                 }})

        final_response         = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_security.login_sec_token,
                                                                             _security.login_token,
                                                                             _security.token_user,
                                                                             _history.id)

        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })

        final_response = make_response(error)
        return final_response

# delete files
@admin.route('/api/delete/<int:id>/files', methods=['GET', 'DELETE'])
@jwt_refresh_token_required
def delete_files(id):
    try:
        header  = request.headers
        UploadFiles().remove_files_from_server(1)


        return "hola"
    except Exception as e:
        print(e)
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        final_response = make_response(error)
        return final_response

# method for set the if of the section
def set_admin_value(section_name):
    options = {
        'Banners'              : 1,
        'How It Works'         : 2,
        'About Us'             : 3,
        'Terms and Conditions' : 4
    }

    return options.get(section_name)

# method for get the string of section name
def get_admin_value(section_name):
    options = {
        1 : 'Banners',
        2 : 'How It Works',
        3 : 'About Us',
        4 : 'Terms and Conditions'
    }

    return options.get(section_name)

# get type file
def get_type_file_string(id):
    return "Image" if id == 1 else "Video"
