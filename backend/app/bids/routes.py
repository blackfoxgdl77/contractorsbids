from flask import request, jsonify, json, make_response, current_app as app
from app.libraries.jwtFunctions import JwtFunctions
from app.bids import bids
import requests, os
from app.models import SecurityToken, LoginHistory, ErrorLog, Post, Bid
from app import db
from datetime import datetime
from flask_jwt_extended import jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt
from app.libraries.encryptInformation import EncryptInformation

# bids active where is participating the user
@bids.route('/api/bids/make/offer', methods=['POST'])
@jwt_refresh_token_required
def make_an_offer():
    try:
        body    = request.get_json()
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        postrec   = Post.query.filter_by(id=body['postId']).first()
        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()

        if _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif postrec is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The post is invalid. Please verify contacting contractorsbids Support Team.' })
        elif body['offer'] is None or body['offer'] == 0:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'messaeg' : 'The  amount offerd is invalid!' })
        else:
            _token_sec = EncryptInformation().generate_token_sec(_security.sec_token.email, _security.user_id, _security.sec_token.name)
            _token_log = EncryptInformation().generate_token_login(_security.sec_token.email, _security.sec_token.username, _security.user_id)
            _token_num = EncryptInformation().encrypt_user_id(_security.user_id, _history.date_login)

            #update sec token table
            _security.login_sec_token = _token_sec
            _security.login_token     = _token_log
            _security.token_user      = _token_num
            db.session.commit()

            #update login history table
            _history.token_login = _token_log
            _history.token_sec   = _token_sec
            db.session.commit()

            old_bids = Bid.query.filter_by(bid_post=postrec.id).update(dict(bid_status=0))
            db.session.commit()

            bids = Bid(bid_post=body['postId'],
                       bid_money=body['offer'],
                       bid_user=_security.user_id)
            db.session.add(bids)
            db.session.commit()
            db.session.flush()

            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'message' : 'The Offer has been done successfully.',
                                 'header'  : 'Offer done successfully.',
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'    : {
                                    'post'  : postrec.id,
                                    'money' : bids.bid_money,
                                    'user'  : bids.user_bid.username
                                 }})

        final_response         = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_token_sec,
                                                                             _token_log,
                                                                             _token_num,
                                                                             _history.id)

        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        response = make_response(error)
        return response

#bids active where the user is participating
@bids.route('/api/bids/<string:id>/active', methods=['GET'])
@jwt_refresh_token_required
def bids_active(id):
    try:
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=id,
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()
        bids      = Bid.query.filter_by(bid_user=_security.user_id, bid_status=1)

        if bids is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The user does not exists or does not have any bid. Please contact contractorsbids Support Team.' })
        elif _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif id is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'Data Invalid! Please contact contractorsbids Support Team!' })
        else:
            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'message' : 'The records has been recovery successfully.',
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'    : [
                                    {
                                        'id'              : bid.id,
                                        'project_id'      : bid.bid_post,
                                        'project_name'    : bid.post_bid.post_name,
                                        'category'        : bid.post_bid.post_category.name,
                                        'auctioneer_id'   : bid.post_bid.user_post.id,
                                        'auctioneer_user' : bid.post_bid.user_post.username,
                                        'current_user'    : bid.user_bid.username,
                                        'bid_ammount'     : bid.bid_money,
                                        'status_bid'      : bid.bid_status
                                    } for bid in bids
                                 ]})

        final_response = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_security.login_sec_token,
                                                                             _security.login_token,
                                                                             _security.token_user,
                                                                             _history.id)

        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' :  app.config['EXCEPTION_MESSAGE_ERROR'] })
        response = make_response(error)
        return response

# bids inactive where the user was participating
@bids.route('/api/bids/<string:id>/end', methods=['GET'])
@jwt_refresh_token_required
def bids_inactive(id):
    try:
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=id,
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()
        bids      = Bid.query.filter_by(bid_user=_security.user_id, bid_status=2).\
            join(Post, Post.id==Bid.bid_post).filter_by(user_id=_security.user_id, post_status=0).order_by(Post.date_created)

        if bids is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The user does not exists or does not have any bid. Please contact contractorsbids Support Team.' })
        elif _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif id is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'Data Invalid! Please contact contractorsbids Support Team!' })
        else:
            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'message' : 'The records has been recovery successfully.',
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'    : [
                                    {
                                        'id'              : bid.id,
                                        'project_id'      : bid.bid_post,
                                        'project_name'    : bid.post_bid.post_name,
                                        'category'        : bid.post_bid.post_category.name,
                                        'bid_winner'      : bid.bid_money,
                                        'user_winner'     : bid.user_bid.username,
                                        'auctioneer_id'   : bid.post_bid.user_post.id,
                                        'auctioneer_user' : bid.post_bid.user_post.username,
                                        'status_bid'      : bid.bid_status,
                                    } for bid in bids
                                 ]})

        final_response = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_security.login_sec_token,
                                                                             _security.login_token,
                                                                             _security.token_user,
                                                                             _history.id)


        return final_response
    except Exception as e:
        error = jsonify({ 'code'     : 400,
                          'status'   : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        response = make_response(error)
        return response

# offers to my projects
@bids.route('/api/bids/<string:id>/open/own', methods=['GET'])
@jwt_refresh_token_required
def my_projects_bids(id):
    try:
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security  = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=id,
                                                    status=1).first()
        _history   = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()
        bids_posts = Bid.query.filter_by(bid_status=1).\
            join(Post, Post.id==Bid.bid_post).filter_by(user_id=_security.user_id, post_status=1).order_by(Post.date_created)

        if _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif id is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'Data Invalid! Please contact contractorsbids Support Team!' })
        elif bids_posts is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The user does not exists or does not have any bid. Please contact contractorsbids Support Team.' })
        else:
            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'message' : 'The records has been recovery successfully.',
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'    : [
                                    {
                                        'post'         : bid_post.bid_post,
                                        'bid'          : bid_post.id,
                                        'post_name'    : bid_post.post_bid.post_name,
                                        'status'       : bid_post.bid_status,
                                        'offer'        : bid_post.bid_money,
                                        'current_user' : bid_post.user_bid.username,
                                        'category'     : bid_post.post_bid.post_category.name
                                    } for bid_post in bids_posts
                                 ]})

        final_response = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_security.login_sec_token,
                                                                             _security.login_token,
                                                                             _security.token_user,
                                                                             _history.id)


        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        response = make_response(error)
        return response

# end offers to my projects
@bids.route('/api/bids/<string:id>/end/own', methods=['GET'])
@jwt_refresh_token_required
def end_my_projects_bids(id):
    try:
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security  = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=id,
                                                    status=1).first()
        _history   = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()
        bids_posts = Bid.query.filter_by(bid_status=2).\
            join(Post, Post.id==Bid.bid_post).filter_by(user_id=_security.user_id, post_status=0).order_by(Post.date_created)

        if _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif id is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'Data Invalid! Please contact contractorsbids Support Team!' })
        elif bids_posts is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The user does not exists or does not have any bid. Please contact contractorsbids Support Team.' })
        else:
            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'message' : 'The records has been recovery successfully.',
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'    : [
                                    {
                                        'bid'           : bid.id,
                                        'post'         : bid.bid_post,
                                        'post_name'    : bid.post_bid.post_name,
                                        'category'     : bid.post_bid.post_category.name,
                                        'bid_winner'   : bid.bid_money,
                                        'username_won' : bid.user_bid.username,
                                        'owner'        : bid.post_bid.user_post.username,
                                        'status'       : bid.bid_status,
                                        'post_status'  : bid.post_bid.post_status
                                    } for bid in bids_posts
                                 ]})


        final_response = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_security.login_sec_token,
                                                                             _security.login_token,
                                                                             _security.token_user,
                                                                             _history.id)


        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        response = make_response(error)
        return response

# get list of all the offers in the project
@bids.route('/api/get/<int:id>/<string:uid>/projects/bids', methods=['GET'])
@jwt_refresh_token_required
def get_bids_projects(id, uid):
    try:
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=uid,
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()
        bids_post = Bid.query.filter_by(bid_post=id).order_by(Bid.bid_status.desc()).order_by(Bid.date_created.desc())

        if bids_post is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The post bids do not exists or do not have any bid. Please contact contractorsbids Support Team.' })
        elif _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif id is None or uid is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'Request / Data Invalid! Please contact contractorsbids Support Team!' })
        else:
            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'message' : 'The records has been recovery successfully.',
                                 'header'  : 'Records recovery successfully.',
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'    : [
                                    {
                                        'bid'          : bid.id,
                                        'post'         : bid.bid_post,
                                        'name'         : bid.post_bid.post_name,
                                        'offer'        : bid.bid_money,
                                        'username'     : bid.user_bid.username,
                                        'category'     : bid.post_bid.post_category.name,
                                        'status_post'  : bid.post_bid.post_status,
                                        'status'       : bid.bid_status,
                                        'date_created' : bid.date_created
                                    } for bid in bids_post
                                 ]})

        final_response = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_security.login_sec_token,
                                                                             _security.login_token,
                                                                             _security.token_user,
                                                                             _history.id)

        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })

        response = make_response(error)
        return response

# accept bid
@bids.route('/api/bid/accept', methods=['POST'])
@jwt_refresh_token_required
def accept_bid():
    try:
        body    = request.get_json()
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()
        bids_post = Bid.query.filter_by(id=body['bid']).first()

        if _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif bids_post is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The bid do not exists or it is incorrect. Please contact contractorsbids Support Team.' })
        else:
            messages = offer_message(body['status'])

            _token_sec = EncryptInformation().generate_token_sec(_security.sec_token.email, _security.user_id, _security.sec_token.name)
            _token_log = EncryptInformation().generate_token_login(_security.sec_token.email, _security.sec_token.username, _security.user_id)
            _token_num = EncryptInformation().encrypt_user_id(_security.user_id, _history.date_login)

            #update sec token table
            _security.login_sec_token = _token_sec
            _security.login_token     = _token_log
            _security.token_user      = _token_num
            db.session.commit()

            #update login history table
            _history.token_login = _token_log
            _history.token_sec   = _token_sec
            db.session.commit()

            update_bids = Bid.query.filter_by(bid_post=bids_post.bid_post).update(dict(bid_status=0))
            db.session.commit()

            update_post = Post.query.filter_by(id=bids_post.bid_post).update(dict(post_status=0))
            db.session.commit()

            #update to approved or accepted
            bids_post.bid_status = body['status']
            db.session.commit()

            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'message' : messages['message'],
                                 'header'  : messages['headers'],
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'    : {
                                    'post'       : bids_post.bid_post,
                                    'status'     : bids_post.bid_status,
                                    'bid'        : bids_post.id,
                                    'username'   : bids_post.user_bid.username,
                                    'bid_amount' : bids_post.bid_money
                                 }})

        final_response = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_security.login_sec_token,
                                                                             _security.login_token,
                                                                             _security.token_user,
                                                                             _history.id)

        return response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        response = make_response(error)
        return response

def offer_message(status):
    headers = "Bid Accepted" if status == 2 else 'Bid Declined'
    message = "The bid has been accepted." if status == 2 else "The bid has been declined"

    obj = { 'headers' : headers,
            'message' : message }

    return obj
