from flask import jsonify, request, json, make_response, current_app as app
from app.libraries.jwtFunctions import JwtFunctions
from app.payments import payments
from app.models import Post, PaymentHistory, SecurityToken, LoginHistory
from app import db
from datetime import datetime
from app.libraries.encryptInformation import EncryptInformation
from flask_jwt_extended import jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt
import requests

@payments.route('/api/payment/cancel', methods=['POST'])
@jwt_refresh_token_required
def cancel_payment():
    try:
        body    = request.get_json()
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()

        if _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif body is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'Data sent is invalid!' })
        else:
            """_token_sec = EncryptInformation().generate_token_sec(_security.sec_token.email, _security.user_id, _security.sec_token.name)
            _token_log = EncryptInformation().generate_token_login(_security.sec_token.email, _security.sec_token.username, _security.user_id)
            _token_num = EncryptInformation().encrypt_user_id(_security.user_id, _history.date_login)

            #update sec token table
            _security.login_sec_token = _token_sec
            _security.login_token     = _token_log
            _security.token_user      = _token_num
            db.session.commit()

            #update login history table
            _history.token_login = _token_log
            _history.token_sec   = _token_sec
            db.session.commit()"""

            payment = PaymentHistory(user_id=1,
                                     post_id=body['postId'],
                                     amount=body['amount'],
                                     order_number=body['order'],
                                     currency='USD',
                                     status='Canceled',
                                     payment_source="Paypal")

            db.session.add(payment)
            db.session.commit()
            db.session.flush()

            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'message' : 'Payment has been done successfully. Please wait confirmation.',
                                 'header'  : 'Payment successfully.',
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'    : {
                                    'id'       : payment.id,
                                    'post'     : payment.post_id,
                                    'amount'   : payment.amount,
                                    'order'    : payment.order_number,
                                    'currency' : payment.currency,
                                    'status'   : payment.status
                                 }})

        final_response         = make_response(response)
        """final_response.headers = EncryptInformation().create_response_header(_token_sec,
                                                                             _token_log,
                                                                             _token_num,
                                                                             _history.id)"""

        return final_response
    except Exception as e:
        print(e)
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        final_response = make_response(error)

        return final_response

@payments.route('/api/payment/confirm', methods=['POST'])
@jwt_refresh_token_required
def confirm_payment():
    try:
        body    = request.get_json()
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()

        if _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif body is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'Data sent is invalid!'})
        else:
            """_token_sec = EncryptInformation().generate_token_sec(_security.sec_token.email, _security.user_id, _security.sec_token.name)
            _token_log = EncryptInformation().generate_token_login(_security.sec_token.email, _security.sec_token.username, _security.user_id)
            _token_num = EncryptInformation().encrypt_user_id(_security.user_id, _history.date_login)

            #update sec token table
            _security.login_sec_token = _token_sec
            _security.login_token     = _token_log
            _security.token_user      = _token_num
            db.session.commit()

            #update login history table
            _history.token_login = _token_log
            _history.token_sec   = _token_sec
            db.session.commit()"""

            payments = PaymentHistory(user_id=1,
                                      post_id=3,
                                      amount=1,
                                      order_number=body['order_number'],
                                      currency=body['currency'],
                                      status=body['status'],
                                      transaction_id=body['transaction'],
                                      payment_source=body['payment_source'],
                                      given_name=body['payer_name'],
                                      merchant_id=body['merchant_id'],
                                      payer_email=body['payer_email'],
                                      surname=body['payer_surname'])

            db.session.add(payments)
            db.session.commit()
            db.session.flush()

            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'message' : 'Transaction has been completed successfully.',
                                 'header'  : 'Transaction completed!',
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'    : {
                                    'id'             : payments.id,
                                    'user'           : payments.user_id,
                                    'post'           : payments.post_id,
                                    'amount'         : payments.amount,
                                    'orderID'        : payments.order_number,
                                    'currency'       : payments.currency,
                                    'status'         : payments.status,
                                    'transaction'    : payments.transaction_id,
                                    'merchant'       : payments.merchant_id,
                                    'payment_source' : payments.payment_source,
                                    'payer_name'     : payments.given_name,
                                    'payer_email'    : payments.payer_email,
                                    'payer_surname'  : payments.surname,
                                    'date_created'   : payments.date_created
                                 }})


        final_response = make_response(response)
        """final_response.headers = EncryptInformation().create_response_header(_token_sec,
                                                                             _token_log,
                                                                             _token_num,
                                                                             _history.id)"""

        return final_response
    except Exception as e:
        print(e)
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        response = make_response(error)

        return response
