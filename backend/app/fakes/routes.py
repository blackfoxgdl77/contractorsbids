from app.fakes import fakes
from app.libraries.jwtFunctions import JwtFunctions
from flask import jsonify, json, make_response, request, current_app as app
import requests, os
from app.models import ReportFake, SecurityToken, Post, User, LoginHistory
from app import db
from datetime import datetime
from app.libraries.encryptInformation import EncryptInformation
from flask_jwt_extended import jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt

#report job as fake
@fakes.route('/api/fake/<int:idp>/report', methods=['POST'])
@jwt_refresh_token_required
def report_fake(idp):
    try:
        tokens  = request.get_json()
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()
        project   = Post.query.filter_by(id=idp, post_status=1).first()

        if _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif project is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The projects does not exist or is invalid. Please verify the data or contact contractorsbids Support Team.' })
        else:
            user_report = ReportFake.query.filter_by(fake_project_id=project.id, reported_user_id=_security.user_id, status_job=1).count()

            if user_report > 0:
                response = jsonify({ 'code'    : 400,
                                     'status'  : 'ERROR',
                                     'message' : 'You already have reported this project as \'Fake\'' })
            else:
                _token_sec = EncryptInformation().generate_token_sec(_security.sec_token.email, _security.user_id, _security.sec_token.name)
                _token_log = EncryptInformation().generate_token_login(_security.sec_token.email, _security.sec_token.username, _security.user_id)
                _token_num = EncryptInformation().encrypt_user_id(_security.user_id, _history.date_login)

                #update sec token table
                _security.login_sec_token = _token_sec
                _security.login_token     = _token_log
                _security.token_user      = _token_num
                db.session.commit()

                #update login history table
                _history.token_login = _token_log
                _history.token_sec   = _token_sec
                db.session.commit()

                report = ReportFake(fake_project_id = project.id,
                                    reported_user_id = security.user_id,
                                    status_job = 1)

                db.session.add(report)
                db.session.commit()

                response = jsonify({ 'code'    : 200,
                                     'status'  : 'OK',
                                     'message' : 'The project has been resported as \'Fake\' successfully.',
                                     'header'  : 'Project Reported successfully!',
                                     'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                     'data'    : {
                                         's_token' : {
                                             'userS' : tokensec,
                                             'userL' : tokenlog,
                                             'userN' : tokenuser
                                         },
                                        'post'  : project.id
                                     }})

        final_response = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_token_sec,
                                                                             _token_log,
                                                                             _token_num,
                                                                             _history.id)

        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        final_response = make_response(error)
        return final_response

# get all the fakes projects
@fakes.route('/api/fake/get/all', methods=['GET'])
@jwt_refresh_token_required
def fake_get_all():
    try:
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()
        fakes     = ReportFake.query.all()

        if _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif fakes is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'Can\'t be recovery all the fakes projects. If the issue persist, please contact contractorsbids Support Team.'})
        else:
            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'message' : 'Records recovery successfully.',
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'    : [
                                    {
                                        'id'               : fake.id,
                                        'project_id'       : fake.fake_project_id,
                                        'project_name'     : fake.post_fake.post_name,
                                        'project_status'   : fake.post_fake.post_status,
                                        'reported_user_id' : fake.reported_user_id,
                                        'created_by'       : fake.post_fake.user_post.username,
                                        'username'         : fake.user_fake.username,
                                        'country'          : fake.post_fake.post_cities.country_city.name_country,
                                        'city'             : fake.post_fake.post_cities.city_name,
                                        'category'         : fake.post_fake.post_category.name,
                                        'user_status'      : fake.user_fake.is_active,
                                        'status'           : fake.status_job
                                     } for fake in fakes ]})

        final_response         = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_security.login_sec_token,
                                                                             _security.login_token,
                                                                             _security.token_user,
                                                                             _history.id)

        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        final_response = make_response(error)
        return final_response

#inactive / active fake projects
@fakes.route('/api/fake/<int:id>/<int:status>/status', methods=['POST', 'PUT'])
@jwt_refresh_token_required
def fake_status_fake(id, status):
    try:
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()
        fakes     = ReportFake.query.filter_by(id=id).first()

        if fakes is None or status > 2:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The ID / STATUS is invalid. Please, verify the information is correct otherwise contact contractorsbids Support Team.' })
        elif _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        else:
            _token_sec = EncryptInformation().generate_token_sec(_security.sec_token.email, _security.user_id, _security.sec_token.name)
            _token_log = EncryptInformation().generate_token_login(_security.sec_token.email, _security.sec_token.username, _security.user_id)
            _token_num = EncryptInformation().encrypt_user_id(_security.user_id, _history.date_login)

            #update sec token table
            _security.login_sec_token = _token_sec
            _security.login_token     = _token_log
            _security.token_user      = _token_num
            db.session.commit()

            #update login history table
            _history.token_login = _token_log
            _history.token_sec   = _token_sec
            db.session.commit()

            fakes.status_job = status
            db.session.commit()

            message = "Records has been {}".format(status_fake_jobs(status))

            #send email

            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'message' : 'Record updated successfully.',
                                 'header'  : message,
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'    : {
                                    'id'     : id,
                                    'status' : status_fake_jobs(status),
                                    's_token' : {
                                        'userS' : tokensec,
                                        'userL' : tokenlog,
                                        'userN' : tokenuser
                                    }
                                 }})

        final_response = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_token_sec,
                                                                             _token_log,
                                                                             _token_num,
                                                                             _history.id)

        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        final_response = make_response(error)
        return final_response

# delete job
@fakes.route('/api/fake/<int:id>/delete', methods=['POST', 'PUT'])
@jwt_refresh_token_required
def delete_fake(id):
    try:
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_token['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()
        fakes     = ReportFake.query.filter_by(id=id).first()

        if fakes is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'Record invalid. The project is Blocked / Inactive / Deleted. Please contact the admin of contractorsbids to solve the issue.' })
        elif _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        else:
            _token_sec = EncryptInformation().generate_token_sec(_security.sec_token.email, _security.user_id, _security.sec_token.name)
            _token_log = EncryptInformation().generate_token_login(_security.sec_token.email, _security.sec_token.username, _security.user_id)
            _token_num = EncryptInformation().encrypt_user_id(_security.user_id, _history.date_login)

            #update sec token table
            _security.login_sec_token = _token_sec
            _security.login_token     = _token_log
            _security.token_user      = _token_num
            db.session.commit()

            #update login history table
            _history.token_login = _token_log
            _history.token_sec   = _token_sec
            db.session.commit()

            fakes.status_job = 0
            db.session.commit()

            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'message' : "The project has been blocked successfully.",
                                 'header'  : "Project Blocked Successfully!",
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'    : {
                                    'id'     : id,
                                    'status' : 'Blocked',
                                    's_token' : {
                                        'userS' : tokensec,
                                        'userL' : tokenlog,
                                        'userN' : tokenuser
                                    }
                                }})

        final_response = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_token_sec,
                                                                             _token_log,
                                                                             _token_num,
                                                                             _history.id)

        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        final_response = make_response(error)
        return final_response

def status_fake_jobs(status):
    return "Inactive" if status == 1 else "Active"
